
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = uxx + uyy
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125

    mpiexec -n 1 ./exhaustive_loop6 -m 20 -ms 100 -sf 10 -ts_monitor -ts_adjoint_monitor
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
	PetscInt  m;
  PetscReal c;
	Vec       adj_pre, u_pre, lambda[1];
	Vec       *r;
	PetscReal computed_error_norm, estimated_error_norm, integrated_error_norm,initial_error_norm;
	PetscReal dt_pre, dt_fine;
  PetscInt  actual_steps, steps_fine, max_steps;

  // ts fine for ts_monitor
  TS        ts_fine;
  DM        da_fine;
  Mat       J_fine;
  Vec       u_fine;
  Vec       r_fine;
  Vec       rhs_dis,rhs_exact;
  Vec       r_1, r_3;
} AppCtx;


extern PetscErrorCode ExactRHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode FormInitialSolution(DM,Vec,void*);
extern PetscErrorCode IFunction(TS, PetscReal, Vec, Vec, Vec, void*);
extern PetscErrorCode IJacobian(TS, PetscReal, Vec, Vec, PetscReal, Mat, Mat, void*);
extern PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
extern PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);
extern PetscErrorCode ComputeEn(PetscInt, PetscReal*);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  Vec            u,r,solution;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscErrorCode ierr;
  DM             da;
  PetscReal      ftime,dt,dx,simu_error_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscScalar    one = 1.0;
  PetscReal      dt_coef = 0.5;
	PetscReal 		 norm_initial, En=1.0;
	PetscInt       m = 20;
	PetscInt       steps_fine = 4, max_steps = 100, steps;
  Vec            error_distribution;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-sf",&steps_fine,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
	
  /* Initialize user application context */
	user.m = m;
  user.c = 0.025;
	user.steps_fine = steps_fine;
	user.max_steps = max_steps;
	user.computed_error_norm = 0.0;
	user.estimated_error_norm = 0.0;
	user.integrated_error_norm = 0.0;
	user.initial_error_norm = 0.0;

	/* Create the domain */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);
  /* Create the domian for ts_fine */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&user.da_fine);CHKERRQ(ierr);
  ierr = DMSetFromOptions(user.da_fine);CHKERRQ(ierr);
  ierr = DMSetUp(user.da_fine);CHKERRQ(ierr);

	/* Create the global vector */
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(user.da_fine,&user.u_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_fine);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.u_pre);CHKERRQ(ierr);
  ierr = VecDuplicateVecs(u,max_steps+1,&user.r);
  ierr = VecDuplicate(user.u_fine,&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.rhs_exact);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_1);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_3);CHKERRQ(ierr);

	/* Create and set up TS solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);
	ierr = TSMonitorSet(ts,ts_monitor,&user,NULL);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  /* Create the ts fine solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&user.ts_fine);CHKERRQ(ierr);
  ierr = TSSetDM(user.ts_fine,user.da_fine);CHKERRQ(ierr);
  ierr = TSSetType(user.ts_fine,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(user.ts_fine,user.r_fine,RHSFunction,&user);CHKERRQ(ierr);
  ierr = DMSetMatType(user.da_fine,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(user.da_fine,&user.J_fine);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(user.ts_fine,user.J_fine,user.J_fine,RHSJacobian,&user);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(user.ts_fine,user.steps_fine);CHKERRQ(ierr);
  ierr = TSSetTimeStep(user.ts_fine,user.dt_fine);CHKERRQ(ierr);

  /* Set Jacobian */
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);

	/* set initial condition */
  ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
	dx   = 1.0/m;
  dt   = dt_coef * dx * dx / user.c;
  user.dt_fine = dt / user.steps_fine;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"exhaustive_loop_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);

	/* Solve the problem */
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);

  /* save the results and check */
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"exhaustive_loop_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"exhaustive_loop_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the solution error*/
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"ODE solved to final time =  %gs with final steps =  %D.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);

	/* estimate the solution error */
	ierr = VecDuplicate(u, &user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDuplicate(u, &user.adj_pre);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&error_distribution);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\n Perform the exhausitve loop to estimate the error estimation\n\n");
	ierr = ComputeEn(m*m,&En);CHKERRQ(ierr);
  ierr = TSSetCostGradients(ts,1,user.lambda,NULL);CHKERRQ(ierr);
  ierr = TSAdjointMonitorSet(ts, ts_adj_monitor, &user, NULL);CHKERRQ(ierr);
  ierr = TSMonitorCancel(ts);CHKERRQ(ierr);

	for (int index = 0; index < m*m; index++)
	{
		/* Adjoint section from here */
		ierr = PetscPrintf(PETSC_COMM_WORLD, "cheking %Dth error:\n", index);CHKERRQ(ierr);
		
		ierr = VecZeroEntries(user.lambda[0]);CHKERRQ(ierr);
    ierr = VecSetValues(user.lambda[0], 1, &index, &one, INSERT_VALUES);CHKERRQ(ierr);

    ierr = VecNorm(user.lambda[0], NORM_2, &norm_initial);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitial norm (should be 1) is %g\n", norm_initial);
    ierr = TSAdjointSolve(ts);CHKERRQ(ierr);

		ierr = PetscPrintf(PETSC_COMM_SELF, "\tIntegrated error is %g \n", user.integrated_error_norm);

		/* save the single value */
		ierr = VecSetValues(error_distribution, 1, &index, &user.integrated_error_norm, INSERT_VALUES);CHKERRQ(ierr);

		/* reset some values */
		user.integrated_error_norm = 0.;

		/* reperform the ode simulation, because of the step issue */
		ierr = PetscPrintf(PETSC_COMM_SELF, "\tBring the adjoint solve back to initial step\n");
		ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
	  ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
		ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,u);CHKERRQ(ierr);
	}

  /* Free up the looping memory */
  ierr = VecNorm(error_distribution,NORM_2,&norm_initial);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_SELF,"Computed error norm is %g, resutls is written in error_distribution.dat...\n",norm_initial);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"error_distribution.dat",FILE_MODE_WRITE,&viewer);
	VecView(error_distribution, viewer);
	PetscViewerDestroy(&viewer);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* destroy allocated */
  ierr = VecDestroy(&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDestroy(&user.rhs_exact);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_1);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_3);CHKERRQ(ierr);
  ierr = VecDestroyVecs(max_steps,&user.r);CHKERRQ(ierr);
	ierr = VecDestroy(&user.u_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&error_distribution);CHKERRQ(ierr);
	ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = TSDestroy(&user.ts_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&user.u_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_fine);CHKERRQ(ierr);
  ierr = DMDestroy(&user.da_fine);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_fine);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime, Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscInt       m,local_steps;
  PetscReal      norm_test,local_ftime;
  // PetscViewer    viewer;

	m = appctx->m;
	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\tCache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{
    /* Set initial condition and solve */
    ierr = VecCopy(appctx->u_pre,appctx->u_fine);CHKERRQ(ierr);
    ierr = TSSetTime(appctx->ts_fine,0.0);
    ierr = TSSetTimeStep(appctx->ts_fine,appctx->dt_fine);
    ierr = TSSetStepNumber(appctx->ts_fine,0);
    ierr = TSSetFromOptions(appctx->ts_fine);CHKERRQ(ierr);
    ierr = TSSolve(appctx->ts_fine,appctx->u_fine);CHKERRQ(ierr);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);                                 // save u to u_pre
    ierr = TSGetSolveTime(appctx->ts_fine,&local_ftime);CHKERRQ(ierr);
    ierr = TSGetStepNumber(appctx->ts_fine,&local_steps);CHKERRQ(ierr);

    ierr = RHSFunction(ts,ptime,u,appctx->rhs_dis,ctx);CHKERRQ(ierr);                     // evaluate the rhs discretization = r
    ierr = ExactRHSFunction(ts,ptime,appctx->u_fine,appctx->rhs_exact,ctx);CHKERRQ(ierr); // evaluate the exact rhs discretization = r_1
    ierr = VecAXPBYPCZ(appctx->r_1,1.0,-1.0,0.0,appctx->rhs_exact,appctx->rhs_dis);               // Compute the spatial discretization error r_1 = rhs_exact - rhs_dis                         

    /* Compute and save the temporal and spacial error */
    ierr = VecAXPBYPCZ(appctx->r_3,1.0,-1.0,0.0,appctx->u_fine,u);CHKERRQ(ierr);      // compute the temporal discretization error r_3 = u_fine - u_coarse
    
    // PetscViewerBinaryOpen(PETSC_COMM_WORLD,"r_1.dat",FILE_MODE_WRITE,&viewer);
	  // VecView(r_1, viewer);
	  // PetscViewerDestroy(&viewer);

    // PetscViewerBinaryOpen(PETSC_COMM_WORLD,"r_3.dat",FILE_MODE_WRITE,&viewer);
	  // VecView(r_3, viewer);
	  // PetscViewerDestroy(&viewer);

    ierr = VecNorm(appctx->r_3,NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\ttemporal error norm is %g, ",norm_test);
    ierr = VecNorm(appctx->r_1,NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"spacial error norm is %g. ",norm_test);
    
    ierr = VecAXPY(appctx->r_1, 1.0,appctx->r_3);CHKERRQ(ierr);
    ierr = VecCopy(appctx->r_1,appctx->r[step]);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"Local ts solve end at t = %g with steps = %D. ",local_ftime,local_steps);
    PetscPrintf(PETSC_COMM_SELF,"Cache u_%D, r_%D norm is %g\n",step,step,norm_test);
	}
	return 0;
}

PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscReal      dt, norm1, norm2;

	ierr = TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
    PetscPrintf(PETSC_COMM_SELF, "\tcache the adjoint and dt...\n");
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += -(norm1 + norm2)/2.0 * appctx->dt_pre;
    PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	return 0;
}

/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */

PetscErrorCode ExactRHSFunction(TS ts, PetscReal ftime,Vec U, Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      hx,hy,sx,sy;
  PetscScalar    **uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;
  PetscReal      coef,expo,t;
  PetscReal      x,y;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_SELF," current time is %g ",t);
  expo = -8.0 * c * PETSC_PI * PETSC_PI * t;
  coef = -8.0 * c * PETSC_PI * PETSC_PI * PetscExpReal(expo);
  for (j=ys; j<ys+ym; j++) {
    y = j * hy;
    for (i=xs; i<xs+xm; i++) {
      x = i * hx;
      f[j][i] = coef * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   U - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   J - Jacobian matrix
   Jpre - optionally different preconditioning matrix
   str - flag indicating matrix structure
*/
PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode FormInitialSolution(DM da,Vec U,void* ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscReal      c=user->c;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;
	c  += c;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y, c;
  c = user->c;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscExpReal(-8.0 * PETSC_PI * PETSC_PI *ftime *c) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ComputeEn(PetscInt m, PetscReal *en)
{
	if (m % 2 == 0)
	{
		*en *= 2.0 / PETSC_PI;
		for (int i = 1; i <= m/2 - 1; i++)
		{
			*en *= ((2. * i) / (2. * i - 1.));
		}
	}
	else
	{
		for (int i = 1; i <= (m-1)/2; i++)
		{
			*en *= ((2. * i - 1.) / (2. * i));
		}
	}
	return 0;
}
/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
