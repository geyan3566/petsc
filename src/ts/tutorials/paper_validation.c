
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = c(uxx + uyy)
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
  PetscReal c;
	PetscReal dt_pre, dt_fine;
  PetscInt  actual_steps;
} AppCtx;

extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode FormInitialSolution(DM,Vec,void*);
extern PetscErrorCode IFunction(TS, PetscReal, Vec, Vec, Vec, void*);
extern PetscErrorCode IJacobian(TS, PetscReal, Vec, Vec, PetscReal, Mat, Mat, void*);
extern PetscErrorCode my_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);
extern PetscErrorCode GramSchmidt(PetscInt, Vec*, Vec*);
extern PetscErrorCode ComputeEn(PetscInt, PetscReal*);
extern PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
extern PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode SimpleGramSchmidt(PetscInt,Vec*,Vec*);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  Vec            u,r, solution,l;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscInt       steps;                /* iterations for convergence */
  PetscErrorCode ierr;
  DM             da;
  PetscReal      ftime,dt,dx, simu_error_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscInt       m = 20;            /* size of domain */
  PetscReal      dt_coef = 0.5;
  PetscReal      w = 2.0;
  PetscInt       nb = 3, max_steps = 100, num_tests = 100, num_true = 0;
  Vec            *z, *z_orth;
  PetscRandom    rnd;
  PetscReal      eta, eta_sum, ek, en, lb, ub, pr = 0., one = 1.0;
  PetscScalar    rate = 0.0;
  Vec            validation, vec_epsilon;
  PetscInt       rnd_seed = 0;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-w",&w,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nb",&nb,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nt",&num_tests,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-rs",&rnd_seed,NULL);CHKERRQ(ierr);

  /* Initialize user application context */
  user.c = 0.025;

  /* Create the domain */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);

  /* Initialize some global vectors */
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&l);CHKERRQ(ierr);
  ierr = VecDuplicateVecs(u,nb,&z);CHKERRQ(ierr);
  ierr = VecDuplicateVecs(u,nb,&z_orth);CHKERRQ(ierr);

  /* Set up the TS Solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);

  /* set initial condition and dt */
  ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
	dx   = 1.0/m;
  dt   = dt_coef * dx * dx / user.c;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);

  /* solve the ode problem */
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);

  /* save the results and exact solution */
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_vali_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_vali_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the solution error*/
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_vali_error.dat",FILE_MODE_WRITE,&viewer);CHKERRQ(ierr);
  ierr = VecView(solution, viewer);
  ierr = PetscViewerDestroy(&viewer);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"Final time is %g, and final step is %D.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);
  ierr = VecCopy(solution,l);CHKERRQ(ierr);
  lb = simu_error_norm / w;
  ub = simu_error_norm * w;


  /* test tons of case */
  ierr = VecCreate(PETSC_COMM_WORLD,&validation);CHKERRQ(ierr);
  ierr = VecSetSizes(validation,PETSC_DECIDE,num_tests);CHKERRQ(ierr);
  VecSetFromOptions(validation);
  ierr = VecSet(validation,0.0);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD,&vec_epsilon);CHKERRQ(ierr);
  ierr = VecSetSizes(vec_epsilon,PETSC_DECIDE,num_tests+1);CHKERRQ(ierr);
  VecSetFromOptions(vec_epsilon);
  ierr = VecSet(vec_epsilon,0.0);CHKERRQ(ierr);
  ierr = TSMonitorCancel(ts);CHKERRQ(ierr);
  ierr = PetscRandomCreate(PETSC_COMM_WORLD,&rnd);CHKERRQ(ierr);
  ierr = PetscRandomSetInterval(rnd,-1.0,1.0);CHKERRQ(ierr);
  ierr = ComputeEn((m-1) * (m-1),&en);CHKERRQ(ierr);
  ierr = ComputeEn(nb,&ek);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"En = %g, Ek = %g:\n",en,ek);CHKERRQ(ierr);
  int pos = 0;
  ierr = VecSetValues(vec_epsilon,1,&pos,&simu_error_norm,INSERT_VALUES);CHKERRQ(ierr);
  for (int i = 0; i < num_tests; i++)
  {
    ierr = PetscPrintf(PETSC_COMM_SELF,"%dth test:\n",i);CHKERRQ(ierr);
    ierr = PetscRandomSetSeed(rnd, i+rnd_seed);CHKERRQ(ierr);
    ierr = PetscRandomSeed(rnd);CHKERRQ(ierr);
    for (int index = 0; index < nb; index ++)
    {
      ierr = VecSetRandom(z[index],rnd);CHKERRQ(ierr);
    }
    ierr = SimpleGramSchmidt(nb,z,z_orth);CHKERRQ(ierr);
    eta_sum = 0.0;
    for (int ii = 0; ii < nb; ii++)
    {
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"\t%d th basis: ",ii);CHKERRQ(ierr);
      // ierr = VecNorm(z_orth[ii], NORM_2, &test_norm);CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"z[%d] start norm is %g, ",ii,test_norm);CHKERRQ(ierr);
      // ierr = TSSetCostGradients(ts,1,&z_orth[ii],NULL);CHKERRQ(ierr);
      // ierr = TSAdjointSolve(ts);CHKERRQ(ierr);
      // ierr = VecNorm(z_orth[ii], NORM_2, &test_norm);CHKERRQ(ierr);
      // ierr = PetscPrintf(PETSC_COMM_WORLD,"end norm is %g.",test_norm);CHKERRQ(ierr);
      
      ierr = VecDot(l,z_orth[ii],&eta);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"\tl^T /dot z[%d] =  %g,\n",ii,eta);CHKERRQ(ierr);

      eta = PetscAbsReal(eta);
      eta_sum += eta * eta;

      /* reperform the ode simulation, because of the step issue */
      // ierr = TSReset(ts);CHKERRQ(ierr);
      // ierr = TSSetDM(ts,da);CHKERRQ(ierr);
      // ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
      // ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
      // ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
      // ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
      // ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
      // ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
      // ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
      // ierr = TSSolve(ts,u);CHKERRQ(ierr);
    }
    int posi = i+1;
    eta_sum = PetscSqrtReal(eta_sum);
    eta_sum *= ek / en;
    ierr = PetscPrintf(PETSC_COMM_WORLD,"  epsilon(%d) = %g,\n",nb,eta_sum);CHKERRQ(ierr);
    ierr = VecSetValues(vec_epsilon,1,&posi,&eta_sum,INSERT_VALUES);CHKERRQ(ierr);
    if ( lb <= eta_sum  && eta_sum <= ub)
    {
      ierr = VecSetValues(validation,1,&i,&one,INSERT_VALUES);CHKERRQ(ierr);
      num_true++;
    }
  }

  ierr = PetscPrintf(PETSC_COMM_WORLD,"True simulaition l2 error is %g\n",simu_error_norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Lower bound = %g, upper bound = %g\n",lb,ub);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Number of true = %d, number of tests = %D\n",num_true,num_tests);CHKERRQ(ierr);
  rate = (PetscScalar) num_true/num_tests;
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Pr( ||l|| / w <= epsilon(%d) <= w * ||l|| ) = %g\n",nb,rate);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Write epsilon into file.\n");CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"epsilon.dat",FILE_MODE_WRITE,&viewer);
	ierr = VecView(vec_epsilon, viewer);
	ierr = PetscViewerDestroy(&viewer);
  
  if (nb == 1)
  {
    pr = 1. - 2.0 / (PETSC_PI * w);
  }
  else if (nb == 2)
  {
    pr = 1.0 - PETSC_PI / (4.0 * w * w);
  }
  else if (nb == 3)
  {
    pr = 1.0 - 32./(3. * PETSC_PI * PETSC_PI * w * w * w);
  }
  PetscPrintf(PETSC_COMM_WORLD,"Target probabilty =  %g\n", pr);

  /* free the work space */
  ierr = VecDestroy(&validation);CHKERRQ(ierr);
  ierr = VecDestroy(&vec_epsilon);CHKERRQ(ierr);
  ierr = VecDestroyVecs(nb,&z);CHKERRQ(ierr);
  ierr = VecDestroyVecs(nb,&z_orth);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = VecDestroy(&l);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&rnd);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode SimpleGramSchmidt(PetscInt s, Vec* z, Vec* z_orth)
{
  PetscErrorCode ierr;
  PetscReal      norm;
  Vec            z_copy;

  // VecView(z[0],0);
  // VecView(z[1],0);
  // VecView(z[2],0);

  /* Compute the first vector */
  ierr = VecNorm(z[0],NORM_2,&norm);CHKERRQ(ierr);             // norm(z_0)
  ierr = VecCopy(z[0],z_orth[0]);CHKERRQ(ierr);                // z_orth = z_0
  ierr = VecScale(z_orth[0],1./norm);CHKERRQ(ierr);            // z_orth = z_orth / ||z_orth||
  if (s == 1) return 0;
  /* Compute other orthogonal basis */
  ierr = VecDuplicate(z[0],&z_copy);CHKERRQ(ierr);
  for ( int i = 1; i < s; i++)
  { 
    ierr = VecSet(z_copy,0.0);CHKERRQ(ierr);                     // z_copy = 0.0
    for (int ii = 0; ii < i; ii++)
    {
      ierr = VecDot(z_orth[ii],z[i],&norm);CHKERRQ(ierr);       // norm_ii = <n_ii, v_i>
      ierr = VecAXPY(z_copy,norm,z_orth[ii]);CHKERRQ(ierr);      // z_copy = sum <n_ii , z_i> n_ii ;
    }
    ierr = VecWAXPY(z_orth[i],-1.0,z_copy,z[i]);CHKERRQ(ierr);  // z_orth_i = z_i - z_copy
    ierr = VecNorm(z_orth[i],NORM_2,&norm);CHKERRQ(ierr);
    ierr = VecScale(z_orth[i],1./norm);CHKERRQ(ierr);           // z_orth_i = z_orth_i / ||z_orth_i||
  }
  // PetscPrintf(PETSC_COMM_SELF,"\tCheck orthognality:\n");
  // ierr = VecDot(z_orth[0],z_orth[1],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t0*1 = %g\n", norm);
  // ierr = VecDot(z_orth[1],z_orth[2],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t1*2 = %g\n", norm);
  // ierr = VecDot(z_orth[0],z_orth[2],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t0*2 = %g\n", norm);
  // ierr = VecDestroy(&z_copy);CHKERRQ(ierr);
  return 0;
}
/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */
PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   U - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   J - Jacobian matrix
   Jpre - optionally different preconditioning matrix
   str - flag indicating matrix structure
*/
PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode FormInitialSolution(DM da,Vec U,void* ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscReal      c=user->c;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;
	c  += c;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y, c;
  c = user->c;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscExpReal(-8.0 * PETSC_PI * PETSC_PI *ftime *c) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ComputeEn(PetscInt m, PetscReal *en)
{
  *en = 1.0;
  if (m == 1)
  {
    return 0;
  }
  if (m == 2)
  {
    *en = 2.0 / PETSC_PI;
    return 0;
  }
	if (m % 2 == 0)
	{
		*en *= 2.0 / PETSC_PI;
		for (int i = 1; i <= m/2 - 1; i++)
		{
			*en *= ((2. * i) / (2. * i - 1.));
		}
    *en /= (m-1);
	}
	else
	{
		for (int i = 1; i <= (m-1)/2; i++)
		{
			*en *= ((2. * i - 1.) / (2. * i));
		}
	}
	return 0;
}

/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
