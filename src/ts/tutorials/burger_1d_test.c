
static char help[] = "Get the measurement for the Compressive sensing procedure for global error estimation. \n";
/*
   This program uses the one-dimensional Burger's equation
       u_t = mu*u_xx - u u_x,
   on the domain 0 <= x <= 1, with periodic boundary conditions
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
	PetscInt  m;
	PetscReal mu;
	Vec       adj_pre, u_pre, lambda[1];
	Vec       *r;
	PetscReal integrated_error_norm;
	PetscReal dt_pre, dt_fine;
	PetscInt  actual_steps, steps_fine, max_steps;
	// ts fine for ts_monitor
	TS        ts_fine;
	DM        da_fine;
	Mat       J_fine;
	Vec       u_fine;
	Vec       r_fine;
	Vec       rhs_dis,rhs_exact;
	Vec       r_1, r_3;
} AppCtx;

extern PetscErrorCode ExactRHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
//extern PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
//extern PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode TrueSolution(TS,PetscReal,Vec,AppCtx*);
//extern PetscErrorCode ComputeEn(PetscInt, PetscReal*);

int main(int argc, char **argv)
{
	PetscErrorCode 		ierr;
	TS								ts;
	Vec								u,r,solution;
	Mat								J;
	DM								da;
	PetscReal					ftime,simu_error_norm;
	PetscReal					dt = 0.01;
	AppCtx						user;
	PetscViewer				viewer;
	PetscInt					m = 20;
	PetscInt					steps_fine = 10, max_steps = 100, steps;
	
	/* initialize petsc */
	ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-sf",&steps_fine,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);CHKERRQ(ierr);
	

	/* Initialize some parameters */
	user.m  = m;
	user.mu = 0.01;
	user.steps_fine = steps_fine;
	user.integrated_error_norm = 0.0;

	/* Create the domain for ts */
	ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_PERIODIC,m,1,1,NULL,&da);CHKERRQ(ierr);
	ierr = DMSetFromOptions(da);CHKERRQ(ierr);
	ierr = DMSetUp(da);CHKERRQ(ierr);

	/* Create some global variables */
	// variables for ts solver
	ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&r);CHKERRQ(ierr);


	/* Create the set up the TS Solver */
	ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
	ierr = TSSetDM(ts,da);CHKERRQ(ierr);
	ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
	ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
	ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian,&user);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);


	/* apply the initial condition */
	ierr = TrueSolution(ts,0.0,u,&user);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u,viewer);
	PetscViewerDestroy(&viewer);

	/* Solve the problem and compute the error */
	ierr = TSSolve(ts,u);CHKERRQ(ierr);
	ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
	ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
	user.actual_steps = steps;
	ierr = TrueSolution(ts,ftime,solution,&user);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"Problem is solved to final time =  %gs with final steps =  %D.\n", ftime, steps);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);


	/* Compute the error */
	ierr = VecAXPBY(solution,-1.0,1.0,u);CHKERRQ(ierr);
	ierr = VecNorm(solution,NORM_2,&simu_error_norm);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);
  	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_error.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);


	/* Free the space */
	// destroy the domains
	ierr = DMDestroy(&da);CHKERRQ(ierr);
	// destroy variables for ts
	ierr = TSDestroy(&ts);CHKERRQ(ierr);
	ierr = VecDestroy(&u);CHKERRQ(ierr);
	ierr = VecDestroy(&r);CHKERRQ(ierr);
	ierr = VecDestroy(&solution);CHKERRQ(ierr);
	ierr = MatDestroy(&J);CHKERRQ(ierr);
	ierr = PetscFinalize();
	return ierr;
}

PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
	PetscErrorCode 	ierr;
	AppCtx        	*user=(AppCtx*)ptr;
	DM							da;
	PetscInt        i,Mx,xs,xm;
	PetscReal				hx,sx;
	Vec             localU;
	PetscScalar     *uarray,*f;
	PetscReal       mu = user->mu;
	PetscScalar			ux,uxx;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
	ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
	hx = 2.0/(PetscScalar)Mx; sx = 1.0/(hx*hx);
	

	ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

	ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);CHKERRQ(ierr);

	ierr = DMDAGetCorners(da,&xs,NULL,NULL,&xm,NULL,NULL);CHKERRQ(ierr);

	for (i = xs; i < xs+xm; i++)
	{
		ux  = (uarray[i+1] - uarray[i-1])/(2.*hx);
		uxx = (uarray[i+1] - 2.* uarray[i] + uarray[i-1]) * sx;
		f[i] = mu*uxx - uarray[i]*ux;
	}
	ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}


PetscErrorCode RHSJacobian(TS ts,PetscReal ftime,Vec U,Mat J,Mat Jpre,void *ctx)
{
	PetscErrorCode		ierr;
	AppCtx						*user=(AppCtx*)ctx;
	DM								da;
	DMDALocalInfo			info;
	PetscInt					i;
	PetscReal					hx,sx;
	PetscScalar				vals[3],*uarray;
	Vec								localU;
	PetscReal					mu = user->mu;
	MatStencil				row,col[3];
	PetscInt					nc = 0;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
	hx = 2.0/(PetscReal)(info.mx); sx = 1.0/(hx*hx);

	ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);

	for (i = info.xs; i < info.xs+info.xm; i++)
	{
		nc    	= 0;
		row.i 	= i;
		col[nc].i = i-1; vals[nc++] 	= mu * sx + uarray[i]/(2.0*hx);
		col[nc].i = i;   vals[nc++]   = -2.0 * mu * sx - (uarray[i+1] - uarray[i-1])/(2.0*hx);
		col[nc].i = i+1; vals[nc++]   = mu * sx - uarray[i]/(2.0*hx);
		ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,vals,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);

  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	PetscFunctionReturn(0);
}

PetscErrorCode TrueSolution(TS ts, PetscReal t, Vec U, AppCtx *appctx)
{
	PetscErrorCode			ierr;
	PetscInt						i;
	DM									da;
	DMDALocalInfo				info;
	PetscScalar					*u;
	PetscReal						mu = appctx->mu;
	PetscScalar					x,hx;
	PetscReal						aux1, aux2, aux3, aux4;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	hx = 2.0/(PetscScalar)(info.mx);


	ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

	aux1 = 2.0*mu*PETSC_PI;
	aux2 = PetscExpReal(mu*PETSC_PI*PETSC_PI*t);
	for (i = info.xs; i < info.xs + info.xm; i++)
	{
		x = i * hx;
		aux3 = PetscSinReal(PETSC_PI*x);
		aux4 = PetscCosReal(PETSC_PI*x);
		u[i] = aux1*aux3/(2.*aux2 + aux4);
	}
	ierr = DMDAVecRestoreArray(da,U,&u);
	PetscFunctionReturn(0);
}

PetscErrorCode ExactRHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ctx)
{
	PetscErrorCode		ierr;
	AppCtx						*user=(AppCtx*)ctx;
	DM								da;
	DMDALocalInfo			info;
	PetscInt					i;
	PetscReal					mu = user->mu;
	PetscScalar				*f;
	PetscReal					hx, x, aux1, aux2, aux3, aux4, aux5, aux6,aux7;
	PetscReal					u,ux,uxx;

	PetscFunctionBeginUser;

	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	hx  = 2.0/(PetscScalar)(info.mx);
	ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

	for (i = info.xs; i < info.xs + info.xs + info.xm; i++)
	{
		x = i * hx;
		// compute u
		aux1 = 2.0 * PETSC_PI * mu;
		aux2 = PetscSinReal(PETSC_PI*x);
		aux3 = PetscCosReal(PETSC_PI*x);
		aux4 = PetscExpReal(PETSC_PI*PETSC_PI*mu*ftime);
		u    = aux1 * aux2 / (2.0*aux4 + aux3);
		// compute ux
		aux1 = 2.0 * PETSC_PI * PETSC_PI * mu;
		aux5 = aux2*aux2 + aux3*aux3 + 2.0*aux4*aux3;
		aux6 = aux3 + 2.0*aux4;
		ux   = aux1 * aux5 / (aux6*aux6);
		// compute uxx
		aux1 = 4.0*PETSC_PI*PETSC_PI*PETSC_PI*mu*aux2;
		aux5 = PetscExpReal(2.0*PETSC_PI*PETSC_PI*mu*ftime);
		aux7 = aux2*aux2 + aux3*aux3 + aux4*aux3 - 2.0*aux5;
		uxx = aux1 * aux7 / (aux6*aux6*aux6);
		f[i] = mu*uxx - u*ux;
	}
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}