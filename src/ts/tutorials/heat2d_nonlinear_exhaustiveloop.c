
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = uxx + uyy
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125

    mpiexec -n 1 ./exhaustive_loop6 -m 20 -ms 100 -sf 10 -ts_monitor -ts_adjoint_monitor
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
	PetscInt  m;
	Vec       adj_pre, u_pre, lambda[1];
	Vec       *r;
	PetscReal computed_error_norm, estimated_error_norm, integrated_error_norm,initial_error_norm;
	PetscReal dt_pre;
  PetscInt  actual_steps, max_steps;

  // ts fine for ts_monitor
  Mat       J_aux, J_aux2;
  Vec       rhs_dis;
  Vec       r_1;
} AppCtx;

PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  Vec            u,r,solution;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscErrorCode ierr;
  DM             da;
  PetscReal      ftime,dt,dx,simu_error_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscScalar    one = 1.0;
  PetscReal      dt_coef = 0.5;
	PetscReal 		 norm_initial;
	PetscInt       m = 100;
	PetscInt       max_steps = 100, steps;
  Vec            error_distribution;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
	
  /* Initialize user application context */
	user.m = m;
	user.max_steps = max_steps;
	user.computed_error_norm = 0.0;
	user.estimated_error_norm = 0.0;
	user.integrated_error_norm = 0.0;
	user.initial_error_norm = 0.0;
  dx = 1.0/(m-1);
  dt = dt_coef * dx * dx;


	/* Create the domain */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);

	/* Create the global vector */
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.u_pre);CHKERRQ(ierr);
  ierr = VecDuplicateVecs(u,max_steps+1,&user.r);
  ierr = VecDuplicate(u,&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&user.r_1);CHKERRQ(ierr);

	/* Create and set up TS solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);
	ierr = TSMonitorSet(ts,ts_monitor,&user,NULL);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  /* Set Jacobian */
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&user.J_aux);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&user.J_aux2);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);

	/* set initial condition */
  ierr = ExactSolution(da,u,&user,0.0);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat2d_nonlinear_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);

	/* Solve the problem */
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);

  /* save the results and check */
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat2d_nonlinear_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat2d_nonlinear_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the solution error*/
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"ODE solved to final time =  %gs with final steps =  %d.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);

	/* estimate the solution error */
	ierr = VecDuplicate(u, &user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDuplicate(u, &user.adj_pre);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&error_distribution);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\n Perform the exhausitve loop to estimate the error estimation\n\n");
  ierr = TSSetCostGradients(ts,1,user.lambda,NULL);CHKERRQ(ierr);
  ierr = TSAdjointMonitorSet(ts, ts_adj_monitor, &user, NULL);CHKERRQ(ierr);
  ierr = TSMonitorCancel(ts);CHKERRQ(ierr);

	for (int index = 0; index < m*m; index++)
	{
		/* Adjoint section from here */
		ierr = PetscPrintf(PETSC_COMM_WORLD, "cheking %dth error:\n", index);CHKERRQ(ierr);
		
		ierr = VecZeroEntries(user.lambda[0]);CHKERRQ(ierr);
    ierr = VecSetValues(user.lambda[0], 1, &index, &one, INSERT_VALUES);CHKERRQ(ierr);

    ierr = VecNorm(user.lambda[0], NORM_2, &norm_initial);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitial norm (should be 1) is %g", norm_initial);
    ierr = TSAdjointSolve(ts);CHKERRQ(ierr);

		ierr = PetscPrintf(PETSC_COMM_SELF, "\tIntegrated error is %g \n", user.integrated_error_norm);

		/* save the single value */
		ierr = VecSetValues(error_distribution, 1, &index, &user.integrated_error_norm, INSERT_VALUES);CHKERRQ(ierr);

		/* reset some values */
		user.integrated_error_norm = 0.;

		/* reperform the ode simulation, because of the step issue */
		//ierr = PetscPrintf(PETSC_COMM_SELF, "\tBring the adjoint solve back to initial step\n");
		ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
	  ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
		ierr = ExactSolution(da,u,&user,0.0);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,u);CHKERRQ(ierr);
	}

  /* Free up the looping memory */
  ierr = VecNorm(error_distribution,NORM_2,&norm_initial);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_SELF,"Computed error norm is %g, resutls is written in error_distribution.dat...\n",norm_initial);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat2d_nonlinear_distribution.dat",FILE_MODE_WRITE,&viewer);
	VecView(error_distribution, viewer);
	PetscViewerDestroy(&viewer);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* destroy allocated */
  ierr = VecDestroy(&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_1);CHKERRQ(ierr);
  ierr = VecDestroyVecs(max_steps+1,&user.r);CHKERRQ(ierr);
	ierr = VecDestroy(&user.u_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&error_distribution);CHKERRQ(ierr);
	ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_aux);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_aux2);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime, Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
  PetscReal      norm_test,dt,t;
  //PetscViewer    viewer;
  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);

  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);
	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\tCache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{ 
    ierr = RHSFunction(ts,t,appctx->u_pre,appctx->rhs_dis,appctx);CHKERRQ(ierr);
    ierr = RHSJacobian(ts,t,appctx->u_pre,appctx->J_aux,appctx->J_aux2,appctx);CHKERRQ(ierr);
    // view the data
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"rhs_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = VecView(appctx->rhs_dis, viewer);
    // ierr = PetscViewerDestroy(&viewer);
    
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"jac_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = MatView(appctx->J_aux,viewer);
    // ierr = PetscViewerDestroy(&viewer);

    ierr = MatMult(appctx->J_aux,appctx->rhs_dis,appctx->r_1); // F' * F
    ierr = MatMult(appctx->J_aux,appctx->r_1,appctx->r[step]); // F' * F' * F
    // scale the vector. there is one parameter unkown.
    ierr = VecScale(appctx->r[step],-dt*dt/6);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"r_%d norm is %g\n",step,norm_test);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
	}
	return 0;
}

PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscReal      dt, norm1, norm2;

	ierr = TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += (norm1 + norm2)/2.0 * appctx->dt_pre;
    //PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	return 0;
}

/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */
PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  hx = 1.0/(PetscReal)(Mx-1); sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)(My-1); sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */

  // bot line
  j = 0;
  for (int i = xs; i < xm; i++)
  {
    f[j][i] = 2.0 + 2.0 - 2.0;
  }

  // top line
  j = My-1;
  for (int i = xs; i < xm; i++)
  {
    f[j][i] = 2.0 + 2.0 - 2.0;
  }

  // left boundary
  i = 0;
  for (int j = 1; j < My-1; j++)
  {
    f[j][i] = 2.0 + 2.0 -2.0;
  }

  // right boundary
  i = Mx-1;
  for (int j = 1; j < My-1; j++)
  {
    f[j][i] = 2.0+2.0-2.0;
  }

  // interior points
  for (int j = 1; j < My-1; j++)
  {
    for (int i = 1; i < Mx-1; i++)
    {
      u = uarray[j][i];
      uxx = (-two*u + uarray[j][i+1] + uarray[j][i-1])*sx;
      uyy = (-two*u + uarray[j+1][i] + uarray[j-1][i])*sy;
      f[j][i] = (uxx+uyy) - 2.0;
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  //PetscViewer    viewer;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)(info.mx-1); sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)(info.my-1); sy = 1.0/(hy*hy);


  // bot line
  j = 0;
  for (i = info.xs; i < info.xs+info.xm; i++)
  {
    PetscInt    nc = 0;
    MatStencil  row,col[5];
    PetscScalar val[5];
    row.i = i; row.j = j;
    col[nc].i = i; col[nc].j = j; val[nc++] = 1.0;
    ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
  }

  // top line
  j = info.ys+info.ym-1;
  for (i = info.xs; i < info.xs+info.xm; i++)
  {
    PetscInt    nc = 0;
    MatStencil  row,col[5];
    PetscScalar val[5];
    row.i = i; row.j = j;
    col[nc].i = i; col[nc].j = j; val[nc++] = 1.0;
    ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
  }

  // left line
  i = 0;
  for (j = info.ys+1; j < info.ys+info.ym-1; j++)
  {
    PetscInt    nc = 0;
    MatStencil  row,col[5];
    PetscScalar val[5];
    row.i = i; row.j = j;
    col[nc].i = i; col[nc].j = j; val[nc++] = 1.0;
    ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
  }

  //right line
  i = info.xs+info.xm-1;
  for (j = info.ys+1; j < info.ys+info.ym-1; j++)
  {
    PetscInt    nc = 0;
    MatStencil  row,col[5];
    PetscScalar val[5];
    row.i = i; row.j = j;
    col[nc].i = i; col[nc].j = j; val[nc++] = 1.0;
    ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
  }

  // interior points
  for (j=info.ys+1; j<info.ys+info.ym-1; j++) {
    for (i=info.xs+1; i<info.xs+info.xm-1; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = (-2*sx - 2*sy);
      ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"jac_example.dat",FILE_MODE_WRITE,&viewer);
  // ierr = MatView(J,viewer);
  // ierr = PetscViewerDestroy(&viewer);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)(Mx-1);
  hy = 1.0/(PetscReal)(My-1);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = 2.0*ftime + x*x +y*y;
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
