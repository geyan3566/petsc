
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = c(uxx + uyy)
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
  PetscInt m;
  PetscReal c;
  Vec       adj_pre, u_pre;
  Vec       *r;
	PetscReal dt_pre, dt_fine, integrated_error_norm;
  PetscInt  actual_steps, steps_fine, max_steps;

  // ts fine for ts_monitor
  TS        ts_fine;
  DM        da_fine;
  Mat       J_fine;
  Vec       u_fine;
  Vec       r_fine;
  Vec       rhs_dis,rhs_exact;
  Vec       r_1, r_3;
} AppCtx;

extern PetscErrorCode ExactRHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode FormInitialSolution(DM,Vec,void*);
extern PetscErrorCode IFunction(TS, PetscReal, Vec, Vec, Vec, void*);
extern PetscErrorCode IJacobian(TS, PetscReal, Vec, Vec, PetscReal, Mat, Mat, void*);
extern PetscErrorCode my_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);
extern PetscErrorCode GramSchmidt(PetscInt, Vec*, Vec*);
extern PetscErrorCode ComputeEn(PetscInt, PetscReal*);
extern PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
extern PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode SimpleGramSchmidt(PetscInt,Vec*,Vec*);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  Vec            u,r, solution;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscInt       steps;                /* iterations for convergence */
  PetscErrorCode ierr;
  DM             da;
  PetscReal      ftime,dt,dx, simu_error_norm, test_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscInt       m = 20;            /* size of domain */
  PetscReal      dt_coef = 0.5;
  PetscInt       max_steps = 100, num_tests = 100;
  Vec            *z, *z_orth;
  PetscRandom    rnd;
  PetscReal      eta_sum, ek, en;
  Vec            vec_epsilon;
  PetscInt       rnd_seed = 0, steps_fine = 10;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nt",&num_tests,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-rs",&rnd_seed,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-sf",&steps_fine,NULL);CHKERRQ(ierr);

  /* Initialize user application context */
  user.m = m;
  user.c = 0.025;
	user.steps_fine = steps_fine;
	user.max_steps = max_steps;
	user.integrated_error_norm = 0.0;

  /* Create the domain */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);
  /* Create the domian for ts_fine */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&user.da_fine);CHKERRQ(ierr);
  ierr = DMSetFromOptions(user.da_fine);CHKERRQ(ierr);
  ierr = DMSetUp(user.da_fine);CHKERRQ(ierr);

  /* Initialize some global vectors */
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &user.adj_pre);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(user.da_fine,&user.u_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&user.u_pre);CHKERRQ(ierr);
  ierr = VecDuplicateVecs(u,max_steps+1,&user.r);
  ierr = VecDuplicate(user.u_fine,&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.rhs_exact);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_1);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_3);CHKERRQ(ierr);

  /* Set up the TS Solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);
  ierr = TSMonitorSet(ts,ts_monitor,&user,NULL);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);

  /* Create the ts fine solver */
  ierr = TSCreate(PETSC_COMM_WORLD,&user.ts_fine);CHKERRQ(ierr);
  ierr = TSSetDM(user.ts_fine,user.da_fine);CHKERRQ(ierr);
  ierr = TSSetType(user.ts_fine,TSCN);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(user.ts_fine,user.r_fine,RHSFunction,&user);CHKERRQ(ierr);
  ierr = DMSetMatType(user.da_fine,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(user.da_fine,&user.J_fine);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(user.ts_fine,user.J_fine,user.J_fine,RHSJacobian,&user);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(user.ts_fine,user.steps_fine);CHKERRQ(ierr);
  ierr = TSSetTimeStep(user.ts_fine,user.dt_fine);CHKERRQ(ierr);

  /* set initial condition and dt */
  ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
	dx   = 1.0/m;
  dt   = dt_coef * dx * dx / user.c;
  user.dt_fine = dt / user.steps_fine;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_validation_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);

  /* solve the ode problem */
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);

  /* save the results and exact solution */
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_validation_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_validation_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the solution error*/
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_validation_error.dat",FILE_MODE_WRITE,&viewer);CHKERRQ(ierr);
  ierr = VecView(solution, viewer);
  ierr = PetscViewerDestroy(&viewer);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"Final time is %g, and final step is %D.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);


  /* test tons of case */
  ierr = VecCreate(PETSC_COMM_WORLD,&vec_epsilon);CHKERRQ(ierr);
  ierr = VecSetSizes(vec_epsilon,PETSC_DECIDE,num_tests+1);CHKERRQ(ierr);
  VecSetFromOptions(vec_epsilon);
  ierr = VecSet(vec_epsilon,0.0);CHKERRQ(ierr);
  int pos = 0;

  // set the random seed
  ierr = VecSetValues(vec_epsilon,1,&pos,&simu_error_norm,INSERT_VALUES);CHKERRQ(ierr);
  ierr = PetscRandomCreate(PETSC_COMM_WORLD,&rnd);CHKERRQ(ierr);
  ierr = PetscRandomSetInterval(rnd,-1.0,1.0);CHKERRQ(ierr);
  ierr = ComputeEn((m-1)*(m-1),&en);CHKERRQ(ierr);

  ierr = TSMonitorCancel(ts);CHKERRQ(ierr);
  ierr = TSAdjointMonitorSet(ts, ts_adj_monitor, &user, NULL);CHKERRQ(ierr);
  for (int i = 1; i < num_tests+1; i++)
  {
    ierr = PetscPrintf(PETSC_COMM_SELF,"%dth test:\n",i);CHKERRQ(ierr);

    // create the orthogonal random basis
    ierr = VecDuplicateVecs(u,i,&z);CHKERRQ(ierr);
    ierr = VecDuplicateVecs(u,i,&z_orth);CHKERRQ(ierr);
    ierr = PetscRandomSetSeed(rnd, i+rnd_seed);CHKERRQ(ierr);
    ierr = PetscRandomSeed(rnd);CHKERRQ(ierr);
    for (int index = 0; index < i; index++)
    {
      ierr = VecSetRandom(z[index],rnd);CHKERRQ(ierr);
    }
    ierr = SimpleGramSchmidt(i,z,z_orth);CHKERRQ(ierr);
    
    
    eta_sum = 0.0;
    for (int ii = 0; ii < i; ii++)
    {
      ierr = PetscPrintf(PETSC_COMM_WORLD,"\t%d th basis: ",ii);CHKERRQ(ierr);
      ierr = VecNorm(z_orth[ii], NORM_2, &test_norm);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"z[%d] start norm is %g, ",ii,test_norm);CHKERRQ(ierr);
      ierr = TSSetCostGradients(ts,1,&z_orth[ii],NULL);CHKERRQ(ierr);
      ierr = TSAdjointSolve(ts);CHKERRQ(ierr);
      ierr = VecNorm(z_orth[ii], NORM_2, &test_norm);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD,"end norm is %g.",test_norm);CHKERRQ(ierr);
      
      ierr = PetscPrintf(PETSC_COMM_WORLD,"\tl^T /dot z[%d] =  %g,\n",ii,user.integrated_error_norm);CHKERRQ(ierr);

      eta_sum += user.integrated_error_norm * user.integrated_error_norm;
      user.integrated_error_norm = 0.0;

      /* reperform the ode simulation, because of the step issue */
      ierr = TSReset(ts);CHKERRQ(ierr);
      ierr = TSSetDM(ts,da);CHKERRQ(ierr);
      ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
      ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
      ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
      ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
      ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
      ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
      ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
      ierr = TSSolve(ts,u);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_SELF,"eta_sum is %g. ",eta_sum);CHKERRQ(ierr);
    eta_sum = PetscSqrtReal(eta_sum);
    ierr = ComputeEn(i,&ek);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"En = %g, Ek = %g.",en,ek);CHKERRQ(ierr);
    eta_sum *= ek / en;
    ierr = PetscPrintf(PETSC_COMM_WORLD," Epsilon(%d) = %g,\n",i,eta_sum);CHKERRQ(ierr);
    ierr = VecSetValues(vec_epsilon,1,&i,&eta_sum,INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecDestroyVecs(i,&z);CHKERRQ(ierr);
    ierr = VecDestroyVecs(i,&z_orth);CHKERRQ(ierr);
  }

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_epsilon2.dat",FILE_MODE_WRITE,&viewer);
	ierr = VecView(vec_epsilon, viewer);
	ierr = PetscViewerDestroy(&viewer);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"True simulaition l2 error is %g\n",simu_error_norm);CHKERRQ(ierr);

  /* free the work space */
  ierr = VecDestroy(&user.rhs_dis);CHKERRQ(ierr);
  ierr = VecDestroy(&user.rhs_exact);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_1);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_3);CHKERRQ(ierr);
  ierr = VecDestroyVecs(max_steps,&user.r);CHKERRQ(ierr);
	ierr = VecDestroy(&user.u_pre);CHKERRQ(ierr);
  ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
  ierr = VecDestroy(&vec_epsilon);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = TSDestroy(&user.ts_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&user.u_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_fine);CHKERRQ(ierr);
  ierr = DMDestroy(&user.da_fine);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_fine);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&rnd);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime, Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscInt       local_steps;
  PetscReal      norm_test,local_ftime;
  // PetscViewer    viewer;

	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\tCache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{
    /* Set initial condition and solve */
    ierr = VecCopy(appctx->u_pre,appctx->u_fine);CHKERRQ(ierr);
    ierr = TSSetTime(appctx->ts_fine,0.0);
    ierr = TSSetTimeStep(appctx->ts_fine,appctx->dt_fine);
    ierr = TSSetStepNumber(appctx->ts_fine,0);
    ierr = TSSetFromOptions(appctx->ts_fine);CHKERRQ(ierr);
    ierr = TSSolve(appctx->ts_fine,appctx->u_fine);CHKERRQ(ierr);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);                                 // save u to u_pre
    ierr = TSGetSolveTime(appctx->ts_fine,&local_ftime);CHKERRQ(ierr);
    ierr = TSGetStepNumber(appctx->ts_fine,&local_steps);CHKERRQ(ierr);

    ierr = RHSFunction(ts,ptime,u,appctx->rhs_dis,ctx);CHKERRQ(ierr);                     // evaluate the rhs discretization = r
    ierr = ExactRHSFunction(ts,ptime,appctx->u_fine,appctx->rhs_exact,ctx);CHKERRQ(ierr); // evaluate the exact rhs discretization = r_1
    ierr = VecAXPBYPCZ(appctx->r_1,1.0,-1.0,0.0,appctx->rhs_exact,appctx->rhs_dis);               // Compute the spatial discretization error r_1 = rhs_exact - rhs_dis                         

    /* Compute and save the temporal and spacial error */
    ierr = VecAXPBYPCZ(appctx->r_3,1.0,-1.0,0.0,appctx->u_fine,u);CHKERRQ(ierr);      // compute the temporal discretization error r_3 = u_fine - u_coarse
    
    // PetscViewerBinaryOpen(PETSC_COMM_WORLD,"r_1.dat",FILE_MODE_WRITE,&viewer);
	  // VecView(r_1, viewer);
	  // PetscViewerDestroy(&viewer);

    // PetscViewerBinaryOpen(PETSC_COMM_WORLD,"r_3.dat",FILE_MODE_WRITE,&viewer);
	  // VecView(r_3, viewer);
	  // PetscViewerDestroy(&viewer);

    ierr = VecNorm(appctx->r_3,NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\ttemporal error norm is %g, ",norm_test);
    ierr = VecNorm(appctx->r_1,NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"spacial error norm is %g. ",norm_test);
    
    ierr = VecAXPY(appctx->r_1, 1.0,appctx->r_3);CHKERRQ(ierr);
    ierr = VecCopy(appctx->r_1,appctx->r[step]);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"Local ts solve end at t = %g with steps = %D. ",local_ftime,local_steps);
    PetscPrintf(PETSC_COMM_SELF,"Cache u_%D, r_%D norm is %g\n",step,step,norm_test);
	}
	return 0;
}

PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscReal      dt, norm1, norm2;
  PetscErrorCode ierr;

	ierr = TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
    //PetscPrintf(PETSC_COMM_SELF, "\tcache the adjoint and dt...\n");
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += -(norm1 + norm2)/2.0 * appctx->dt_pre;
    //PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	return ierr;
}


PetscErrorCode SimpleGramSchmidt(PetscInt s, Vec* z, Vec* z_orth)
{
  PetscErrorCode ierr;
  PetscReal      norm;
  Vec            z_copy;

  // VecView(z[0],0);
  // VecView(z[1],0);
  // VecView(z[2],0);

  /* Compute the first vector */
  ierr = VecNorm(z[0],NORM_2,&norm);CHKERRQ(ierr);             // norm(z_0)
  ierr = VecCopy(z[0],z_orth[0]);CHKERRQ(ierr);                // z_orth = z_0
  ierr = VecScale(z_orth[0],1./norm);CHKERRQ(ierr);            // z_orth = z_orth / ||z_orth||
  if (s == 1) return 0;
  /* Compute other orthogonal basis */
  ierr = VecDuplicate(z[0],&z_copy);CHKERRQ(ierr);
  for ( int i = 1; i < s; i++)
  { 
    ierr = VecSet(z_copy,0.0);CHKERRQ(ierr);                     // z_copy = 0.0
    for (int ii = 0; ii < i; ii++)
    {
      ierr = VecDot(z_orth[ii],z[i],&norm);CHKERRQ(ierr);       // norm_ii = <n_ii, v_i>
      ierr = VecAXPY(z_copy,norm,z_orth[ii]);CHKERRQ(ierr);      // z_copy = sum <n_ii , z_i> n_ii ;
    }
    ierr = VecWAXPY(z_orth[i],-1.0,z_copy,z[i]);CHKERRQ(ierr);  // z_orth_i = z_i - z_copy
    ierr = VecNorm(z_orth[i],NORM_2,&norm);CHKERRQ(ierr);
    ierr = VecScale(z_orth[i],1./norm);CHKERRQ(ierr);           // z_orth_i = z_orth_i / ||z_orth_i||
  }
  // PetscPrintf(PETSC_COMM_SELF,"\tCheck orthognality:\n");
  // ierr = VecDot(z_orth[0],z_orth[1],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t0*1 = %g\n", norm);
  // ierr = VecDot(z_orth[1],z_orth[2],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t1*2 = %g\n", norm);
  // ierr = VecDot(z_orth[0],z_orth[2],&norm);
  // PetscPrintf(PETSC_COMM_SELF,"\t0*2 = %g\n", norm);
  // ierr = VecDestroy(&z_copy);CHKERRQ(ierr);
  return 0;
}
/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */


PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode ExactRHSFunction(TS ts, PetscReal ftime,Vec U, Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      hx,hy;
  PetscScalar    **uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;
  PetscReal      coef,expo,t;
  PetscReal      x,y;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_SELF," current time is %g ",t);
  expo = -8.0 * c * PETSC_PI * PETSC_PI * t;
  coef = -8.0 * c * PETSC_PI * PETSC_PI * PetscExpReal(expo);
  for (j=ys; j<ys+ym; j++) {
    y = j * hy;
    for (i=xs; i<xs+xm; i++) {
      x = i * hx;
      f[j][i] = coef * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
/* --------------------------------------------------------------------- */
/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   U - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   J - Jacobian matrix
   Jpre - optionally different preconditioning matrix
   str - flag indicating matrix structure
*/
PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode FormInitialSolution(DM da,Vec U,void* ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscReal      c=user->c;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;
	c  += c;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y, c;
  c = user->c;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscExpReal(-8.0 * PETSC_PI * PETSC_PI *ftime *c) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ComputeEn(PetscInt m, PetscReal *en)
{
  *en = 1.0;
  if (m == 1)
  {
    return 0;
  }
  if (m == 2)
  {
    *en = 2.0 / PETSC_PI;
    return 0;
  }
	if (m % 2 == 0)
	{
		*en *= 2.0 / PETSC_PI;
		for (int i = 1; i <= m/2 - 1; i++)
		{
			*en *= ((2. * i) / (2. * i - 1.));
		}
    *en /= (m-1);
	}
	else
	{
		for (int i = 1; i <= (m-1)/2; i++)
		{
			*en *= ((2. * i - 1.) / (2. * i));
		}
	}
	return 0;
}

/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
