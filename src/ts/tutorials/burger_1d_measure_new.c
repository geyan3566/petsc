
static char help[] = "Get the measurement for the Compressive sensing procedure for global error estimation. \n";
/*
   This program uses the one-dimensional Burger's equation
       u_t = mu*u_xx - u u_x,
   on the domain 0 <= x <= 1, with periodic boundary conditions
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
	PetscInt  m;
	PetscReal mu;
	Vec       adj_pre, u_pre, lambda[1];
	Vec       *r;
	PetscReal integrated_error_norm;
	PetscReal dt_pre, dt_fine;
	PetscInt  actual_steps, max_steps;
	// ts fine for ts_monitor
	Vec       rhs_dis;
	Vec       r_1;
	Mat				J_aux, J_aux2;
} AppCtx;

extern PetscErrorCode ExactRHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
extern PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode TrueSolution(TS,PetscReal,Vec,AppCtx*);
extern PetscErrorCode GetRandomBasis(PetscInt,PetscInt,Vec*,Vec*,PetscRandom);

int main(int argc, char **argv)
{
	PetscErrorCode 		ierr;
	TS								ts;
	Vec								u,r,solution;
	Mat								J;
	DM								da;
	PetscReal					ftime,simu_error_norm,norm_test;
	PetscReal					dt = 0.01;
	AppCtx						user;
	PetscViewer				viewer;
	PetscInt					m = 20, k = 100;
	PetscInt					max_steps = 100, steps, i;
	// variables for random basis
	Vec								*z, *zorth_basis;
	PetscInt					basis_type = 1;
	PetscRandom				rnd;
	// variables for adjoint solve
	Vec								measure;
	
	/* initialize petsc */
	ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-k",&k,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-bt",&basis_type,NULL);CHKERRQ(ierr);
	

	/* Initialize some parameters */
	user.m  = m;
	user.mu = 0.1;
	user.integrated_error_norm = 0.0;

	/* Create the domain for ts */
	ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_PERIODIC,m,1,1,NULL,&da);CHKERRQ(ierr);
	ierr = DMSetFromOptions(da);CHKERRQ(ierr);
	ierr = DMSetUp(da);CHKERRQ(ierr);

	/* Create some global variables */
	// variables for ts solver
	ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.rhs_dis);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.u_pre);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.r_1);CHKERRQ(ierr);
	ierr = VecDuplicateVecs(u,max_steps+1,&user.r);

	// variables for adjoind save
	ierr = VecDuplicateVecs(u,k,&z);CHKERRQ(ierr);
	ierr = VecDuplicateVecs(u,k,&zorth_basis);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.adj_pre);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&measure);CHKERRQ(ierr);
	ierr = VecSetSizes(measure,PETSC_DECIDE,k);CHKERRQ(ierr);
	ierr = VecSetFromOptions(measure);CHKERRQ(ierr);

	/* Create the set up the TS Solver */
	ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
	ierr = TSSetDM(ts,da);CHKERRQ(ierr);
	ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
	ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
	ierr = TSMonitorSet(ts,ts_monitor,&user,NULL);CHKERRQ(ierr);
	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
	ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
	ierr = DMCreateMatrix(da,&user.J_aux);CHKERRQ(ierr);
	ierr = DMCreateMatrix(da,&user.J_aux2);CHKERRQ(ierr);
	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian,&user);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

	/* apply the initial condition */
	ierr = TrueSolution(ts,0.0,u,&user);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u,viewer);
	PetscViewerDestroy(&viewer);

	/* Solve the problem and compute the error */
	ierr = TSSolve(ts,u);CHKERRQ(ierr);
	ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
	ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
	user.actual_steps = steps;
	ierr = TrueSolution(ts,ftime,solution,&user);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"Problem is solved to final time =  %gs with final steps =  %D.\n", ftime, steps);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the error */
	ierr = VecAXPBY(solution,-1.0,1.0,u);CHKERRQ(ierr);
	ierr = VecNorm(solution,NORM_2,&simu_error_norm);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);


	/* Compute the orthogonal basis*/
	ierr = PetscRandomCreate(PETSC_COMM_WORLD,&rnd);CHKERRQ(ierr);
	ierr = PetscRandomSetInterval(rnd,-1.0,1.0);CHKERRQ(ierr);
	ierr = PetscRandomSetFromOptions(rnd);CHKERRQ(ierr);
	ierr = GetRandomBasis(basis_type,k,z,zorth_basis,rnd);CHKERRQ(ierr);

	/* set the adjoint solve */
	ierr = TSAdjointMonitorSet(ts,ts_adj_monitor,&user,NULL);CHKERRQ(ierr);
	ierr = TSMonitorCancel(ts);CHKERRQ(ierr);
	/* Get measurement */
	for (i = 0; i < k; i++)
	{
		PetscPrintf(PETSC_COMM_SELF,"%d th measurement:",i);
		/* adjoind solve */
		ierr = TSSetCostGradients(ts,1,&zorth_basis[i],NULL);CHKERRQ(ierr);
		ierr = VecNorm(zorth_basis[i],NORM_2,&norm_test);CHKERRQ(ierr);
		ierr = TSAdjointSolve(ts);CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_SELF,"\tintegrated error is %g\n",user.integrated_error_norm);
		/* save the measurement */
		ierr = VecSetValues(measure,1,&i,&user.integrated_error_norm,INSERT_VALUES);CHKERRQ(ierr);
		user.integrated_error_norm = 0.0;

		/* bring the ts solver to end stage */
		ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
	  ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
		ierr = TrueSolution(ts,0.0,u,&user);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,u);CHKERRQ(ierr);
	}

	/* save the measurements */
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_measurement.dat",FILE_MODE_WRITE,&viewer);
	VecView(measure, viewer);
	PetscViewerDestroy(&viewer);

	/* Free the space */
	// destroy the domains
	ierr = DMDestroy(&da);CHKERRQ(ierr);

	// destroy variables for ts
	ierr = TSDestroy(&ts);CHKERRQ(ierr);
	ierr = VecDestroy(&u);CHKERRQ(ierr);
	ierr = VecDestroy(&r);CHKERRQ(ierr);
	ierr = VecDestroy(&solution);CHKERRQ(ierr);
	ierr = MatDestroy(&J);CHKERRQ(ierr);

	// destroy variables for adjoint
	ierr = VecDestroy(&user.rhs_dis);CHKERRQ(ierr);
	ierr = VecDestroy(&user.r_1);CHKERRQ(ierr);
	ierr = VecDestroy(&user.u_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
	ierr = VecDestroyVecs(max_steps,&user.r);CHKERRQ(ierr);
	ierr = MatDestroy(&user.J_aux);CHKERRQ(ierr);
	ierr = MatDestroy(&user.J_aux2);CHKERRQ(ierr);

	// free orthogonal basis
	ierr = VecDestroy(&measure);
	ierr = VecDestroyVecs(k,&z);CHKERRQ(ierr);
	ierr = VecDestroyVecs(k,&zorth_basis);CHKERRQ(ierr);
	ierr = PetscRandomDestroy(&rnd);CHKERRQ(ierr);

	ierr = PetscFinalize();
	return ierr;
}

PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
	PetscErrorCode 	ierr;
	AppCtx        	*user=(AppCtx*)ptr;
	DM							da;
	PetscInt        i,Mx,xs,xm;
	PetscReal				hx,sx;
	Vec             localU;
	PetscScalar     *uarray,*f;
	PetscReal       mu = user->mu;
	PetscReal				ux,uxx;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
	ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
	hx = 2.0/(PetscScalar)Mx; sx = hx*hx;
	

	ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

	ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);CHKERRQ(ierr);

	ierr = DMDAGetCorners(da,&xs,NULL,NULL,&xm,NULL,NULL);CHKERRQ(ierr);

	for (i = xs; i < xs+xm; i++)
	{
		ux  = (uarray[i+1] - uarray[i-1])/(2.*hx);
		uxx = (uarray[i+1] - 2.* uarray[i] + uarray[i-1]) / sx;
		f[i] = mu*uxx - uarray[i]*ux;
	}
	ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}


PetscErrorCode RHSJacobian(TS ts,PetscReal ftime,Vec U,Mat J,Mat Jpre,void *ctx)
{
	PetscErrorCode		ierr;
	AppCtx						*user=(AppCtx*)ctx;
	DM								da;
	DMDALocalInfo			info;
	PetscInt					i;
	PetscReal					hx,sx;
	PetscScalar				vals[3],*uarray;
	Vec								localU;
	PetscReal					mu = user->mu;
	MatStencil				row,col[3];
	PetscInt					nc = 0;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
	hx = 2.0/(PetscScalar)(info.mx); sx = hx*hx;

	ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);

	for (i = info.xs; i < info.xs+info.xm; i++)
	{
		nc    	= 0;
		row.i 	= i;
		col[nc].i = i-1; vals[nc++] 	= mu / sx + uarray[i]/(2.0*hx);
		col[nc].i = i;   vals[nc++]   = -2.0 * mu / sx - (uarray[i+1] - uarray[i-1])/(2.0*hx);
		col[nc].i = i+1; vals[nc++]   = mu / sx - uarray[i]/(2.0*hx);
		ierr = MatSetValuesStencil(J,1,&row,nc,col,vals,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);

  ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	PetscFunctionReturn(0);
}

PetscErrorCode TrueSolution(TS ts, PetscReal t, Vec U, AppCtx *appctx)
{
	PetscErrorCode			ierr;
	PetscInt						i;
	DM									da;
	DMDALocalInfo				info;
	PetscScalar					*u;
	PetscReal						mu = appctx->mu;
	PetscScalar					x,hx;
	PetscReal						aux1, aux2, aux3, aux4;

	PetscFunctionBeginUser;
	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	hx = 2.0/(PetscScalar)(info.mx);


	ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

	aux1 = 2.0*mu*PETSC_PI;
	aux2 = PetscExpReal(mu*PETSC_PI*PETSC_PI*t);
	for (i = info.xs; i < info.xs + info.xm; i++)
	{
		x = i * hx;
		aux3 = PetscSinReal(PETSC_PI*x);
		aux4 = PetscCosReal(PETSC_PI*x);
		u[i] = aux1*aux3/(2.*aux2 + aux4);
	}
	ierr = DMDAVecRestoreArray(da,U,&u);
	PetscFunctionReturn(0);
}

PetscErrorCode ExactRHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ctx)
{
	PetscErrorCode		ierr;
	AppCtx						*user=(AppCtx*)ctx;
	DM								da;
	DMDALocalInfo			info;
	PetscInt					i;
	PetscReal					mu = user->mu;
	PetscScalar				*f;
	PetscReal					hx, x, aux1, aux2, aux3, aux4, aux5, aux6,aux7;
	PetscReal					u,ux,uxx;

	PetscFunctionBeginUser;

	ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
	ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
	hx  = 2.0/(PetscScalar)(info.mx);
	ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

	for (i = info.xs; i < info.xs + info.xs + info.xm; i++)
	{
		x = i * hx;
		// compute u
		aux1 = 2.0 * PETSC_PI * mu;
		aux2 = PetscSinReal(PETSC_PI*x);
		aux3 = PetscCosReal(PETSC_PI*x);
		aux4 = PetscExpReal(PETSC_PI*PETSC_PI*mu*ftime);
		u    = aux1 * aux2 / (2.0*aux4 + aux3);
		// compute ux
		aux1 = 2.0 * PETSC_PI * PETSC_PI * mu;
		aux5 = aux2*aux2 + aux3*aux3 + 2.0*aux4*aux3;
		aux6 = aux3 + 2.0*aux4;
		ux   = aux1 * aux5 / (aux6*aux6);
		// compute uxx
		aux1 = 4.0*PETSC_PI*PETSC_PI*PETSC_PI*mu*aux2;
		aux5 = PetscExpReal(2.0*PETSC_PI*PETSC_PI*mu*ftime);
		aux7 = aux2*aux2 + aux3*aux3 + aux4*aux3 - 2.0*aux5;
		uxx = aux1 * aux7 / (aux6*aux6*aux6);
		f[i] = mu*uxx - u*ux;
	}
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}


PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime, Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
  PetscReal      norm_test,dt,t;
  //PetscViewer    viewer;
  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);
	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\tCache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{ 
    ierr = RHSFunction(ts,t,appctx->u_pre,appctx->rhs_dis,appctx);CHKERRQ(ierr);
    ierr = RHSJacobian(ts,t,appctx->u_pre,appctx->J_aux,appctx->J_aux2,appctx);CHKERRQ(ierr);
    // view the data
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"rhs_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = VecView(appctx->rhs_dis, viewer);
    // ierr = PetscViewerDestroy(&viewer);
    
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"jac_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = MatView(appctx->J_aux,viewer);
    // ierr = PetscViewerDestroy(&viewer);

    ierr = MatMult(appctx->J_aux,appctx->rhs_dis,appctx->r_1); // F' * F
    ierr = MatMult(appctx->J_aux,appctx->r_1,appctx->r[step]); // F' * F' * F
    // scale the vector. there is one parameter unkown.
    ierr = VecScale(appctx->r[step],-dt*dt/6);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"r_%D norm is %g\n",step,norm_test);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
	}
	return 0;
}

PetscErrorCode GetRandomBasis(PetscInt bt, PetscInt k, Vec *z, Vec *zorth, PetscRandom rnd)
{
	PetscErrorCode			ierr;
	PetscInt 						i,j,r,nrow;
	PetscViewer					viewer;
	PetscReal						norm_test;
	Mat									basis;
	Vec									z_copy;
	PetscScalar					val;


	ierr = VecSetRandom(z[0],rnd);CHKERRQ(ierr);
	ierr = VecNorm(z[0],NORM_2,&norm_test);CHKERRQ(ierr);
	ierr = VecScale(z[0],1./norm_test);CHKERRQ(ierr);
	ierr = VecCopy(z[0],zorth[0]);CHKERRQ(ierr);

	ierr = VecDuplicate(z[0],&z_copy);
	ierr = VecGetSize(zorth[0],&nrow);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&basis);CHKERRQ(ierr);
	ierr = MatSetSizes(basis,PETSC_DECIDE,PETSC_DECIDE,nrow,k);CHKERRQ(ierr);
	ierr = MatSetType(basis,MATAIJ);CHKERRQ(ierr);
	ierr = MatSetUp(basis);CHKERRQ(ierr);

	i = 0;
	for (r = 0; r < nrow; r++)
	{
		ierr = VecGetValues(zorth[0],1,&r,&val);
		ierr = MatSetValues(basis,1,&r,1,&i,&val,INSERT_VALUES);CHKERRQ(ierr);
	}

	if (bt == 1)
	{
		for (i = 1; i < k; i++)
		{
			ierr = VecSetRandom(z[i],rnd);CHKERRQ(ierr);
			ierr = VecCopy(z[i],z_copy);CHKERRQ(ierr);
			for (j = 0; j < i; j++)
			{
				ierr = VecDot(z_copy,zorth[j],&norm_test);CHKERRQ(ierr);
				ierr = VecAXPY(z[i],-norm_test,zorth[j]);CHKERRQ(ierr);
			}
			ierr = VecNorm(z[i],NORM_2,&norm_test);CHKERRQ(ierr);
			ierr = VecScale(z[i],1./norm_test);CHKERRQ(ierr);
			ierr = VecCopy(z[i],zorth[i]);CHKERRQ(ierr);
			
			for (r = 0; r < nrow; r++)
			{
				ierr = VecGetValues(zorth[i],1,&r,&val);
				ierr = MatSetValues(basis,1,&r,1,&i,&val,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	MatAssemblyBegin(basis,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(basis,MAT_FINAL_ASSEMBLY);

	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"burger_1d_orth_basis.dat",FILE_MODE_WRITE,&viewer);
	MatView(basis, viewer);
	PetscViewerDestroy(&viewer);


	// PetscPrintf(PETSC_COMM_SELF,"Check orhogonality: \n");
	// VecDot(zorth[0], zorth[3],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[1], zorth[2],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[3], zorth[4],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[0], zorth[4],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[2], zorth[3],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// PetscFree(row_index);
	VecDestroy(&z_copy);
	return 0;
}

PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscReal      dt, norm1, norm2;

	TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
    // PetscPrintf(PETSC_COMM_SELF, "\tcache the adjoint and dt...\n");
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += (norm1 + norm2)/2.0 * appctx->dt_pre;
    //PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	return 0;
}