
static char help[] = "Get the measurement for the Compressive sensing procedure for global error estimation. \n";
/*
   This program uses the two dimensional heat equation
       u_t = c( u_xx + u_yy),
   on the domain 0 <= x <= 1, 0 <= y <= 1, with periodic boundary conditions
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

typedef struct {
	PetscInt  m;
	PetscReal c;
	Vec       adj_pre, u_pre, lambda[1];
	Vec       *r;
	PetscReal integrated_error_norm;
	PetscReal dt_pre, dt_fine;
	PetscInt  actual_steps, max_steps;
	// ts fine for ts_monitor
	Vec       rhs_dis;
	Vec       r_1;
	Mat		  J_aux, J_aux2;
} AppCtx;


PetscErrorCode ExactRHSFunction(TS,PetscReal,Vec,Vec,void*);
PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
PetscErrorCode ts_monitor(TS, PetscInt, PetscReal, Vec, void*);
PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
PetscErrorCode TrueSolution(TS,PetscReal,Vec,AppCtx*);
PetscErrorCode GetRandomBasis(PetscInt,PetscInt,Vec*,Vec*,PetscRandom);

int main(int argc, char **argv)
{
	// variables for simulation
	AppCtx user;
	PetscErrorCode ierr;
	TS ts;
	Vec u, r, solution;
	Mat J;
	DM da;
	PetscInt m = 20, k = 100;
	PetscInt max_steps = 100, steps, i;
	PetscReal ftime, dt_coef, dt, dx, simu_error_norm, norm_test;

	// variables for random basis
	Vec *z, *zorth_basis;
	PetscInt basis_type = 1;
	PetscRandom rnd;
	// variables for adjoint solve
	Vec measure;
	// utility
	PetscViewer viewer;

	/* Initialize petsc*/
	ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-k",&k,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(NULL,NULL,"-bt",&basis_type,NULL);CHKERRQ(ierr);

   /* Initialize user application context */
	user.m = m;
  user.c = 0.025;
	user.integrated_error_norm = 0.0;

  /*create the domain for ts*/
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);

  /*Create some global variables*/
  // variables for ts solver
	ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&solution);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
	// variabled for monitor
	ierr = VecDuplicate(u,&user.rhs_dis);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.r_1);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.u_pre);CHKERRQ(ierr);
	ierr = VecDuplicateVecs(u,max_steps+1,&user.r);
	// variables for adjoind save
	ierr = VecDuplicateVecs(u,k,&z);CHKERRQ(ierr);
	ierr = VecDuplicateVecs(u,k,&zorth_basis);CHKERRQ(ierr);
	ierr = VecDuplicate(u,&user.adj_pre);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&measure);CHKERRQ(ierr);
	ierr = VecSetSizes(measure,PETSC_DECIDE,k);CHKERRQ(ierr);
	ierr = VecSetFromOptions(measure);CHKERRQ(ierr);

	/* Create the set up the TS Solver */
	ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
	ierr = TSSetDM(ts,da);CHKERRQ(ierr);
	ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
	ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
	ierr = TSMonitorSet(ts,ts_monitor,&user,NULL);CHKERRQ(ierr);
	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
	ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&user.J_aux);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&user.J_aux2);CHKERRQ(ierr);
	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian,&user);CHKERRQ(ierr);
	ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

	/* apply the initial condition */
	ierr = TrueSolution(ts,0.0,u,&user);CHKERRQ(ierr);
	dx = 1./m;
	dt = dt_coef * dx * dx / user.c;
	ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u,viewer);
	PetscViewerDestroy(&viewer);

  /* Solve the problem and compute the error */
	ierr = TSSolve(ts,u);CHKERRQ(ierr);
	ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
	ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
	user.actual_steps = steps;
	ierr = TrueSolution(ts,ftime,solution,&user);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"Problem is solved to final time =  %gs with final steps =  %D.\n", ftime, steps);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

  /* Compute the error */
	ierr = VecAXPBY(solution,-1.0,1.0,u);CHKERRQ(ierr);
	ierr = VecNorm(solution,NORM_2,&simu_error_norm);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);

  /*setup the orthogonal basis*/
	ierr = PetscRandomCreate(PETSC_COMM_WORLD,&rnd);CHKERRQ(ierr);
	ierr = PetscRandomSetInterval(rnd,-1.0,1.0);CHKERRQ(ierr);
	ierr = PetscRandomSetFromOptions(rnd);CHKERRQ(ierr);
	ierr = GetRandomBasis(basis_type,k,z,zorth_basis,rnd);CHKERRQ(ierr);

   /*setup the adjoint solve*/
	ierr = TSAdjointMonitorSet(ts,ts_adj_monitor,&user,NULL);CHKERRQ(ierr);
	ierr = TSMonitorCancel(ts);CHKERRQ(ierr);
	/* Get measurement */
	for (i = 0; i < k; i++)
	{
		PetscPrintf(PETSC_COMM_SELF,"%d th measurement: ",i);
		/* adjoind solve */
		ierr = TSSetCostGradients(ts,1,&zorth_basis[i],NULL);CHKERRQ(ierr);
		ierr = VecNorm(zorth_basis[i],NORM_2,&norm_test);CHKERRQ(ierr);
		ierr = TSAdjointSolve(ts);CHKERRQ(ierr);

		ierr = PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", user.integrated_error_norm);
		/* save the measurement */
		ierr = VecSetValues(measure,1,&i,&user.integrated_error_norm,INSERT_VALUES);CHKERRQ(ierr);
		user.integrated_error_norm = 0.0;

		/* bring the ts solver to end stage */
		ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
	  ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
		ierr = TrueSolution(ts,0.0,u,&user);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,u);CHKERRQ(ierr);
	}

   /* save the measurements */
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_measurement.dat",FILE_MODE_WRITE,&viewer);
	VecView(measure, viewer);
	PetscViewerDestroy(&viewer);

   /* Free the space */
	// destroy the domains
	ierr = DMDestroy(&da);CHKERRQ(ierr);

	// destroy variables for ts
	ierr = TSDestroy(&ts);CHKERRQ(ierr);
	ierr = VecDestroy(&u);CHKERRQ(ierr);
	ierr = VecDestroy(&r);CHKERRQ(ierr);
	ierr = VecDestroy(&solution);CHKERRQ(ierr);
	ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_aux);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_aux2);CHKERRQ(ierr);

	// destroy variables for ts_fine
	ierr = VecDestroy(&user.rhs_dis);CHKERRQ(ierr);
	ierr = VecDestroy(&user.r_1);CHKERRQ(ierr);
	ierr = VecDestroy(&user.u_pre);CHKERRQ(ierr);
	ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
	ierr = VecDestroyVecs(max_steps,&user.r);CHKERRQ(ierr);

	// free orthogonal basis
	ierr = VecDestroy(&measure);
	ierr = VecDestroyVecs(k,&z);CHKERRQ(ierr);
	ierr = VecDestroyVecs(k,&zorth_basis);CHKERRQ(ierr);
	ierr = PetscRandomDestroy(&rnd);CHKERRQ(ierr);

	ierr = PetscFinalize();
	return ierr;
}

PetscErrorCode ExactRHSFunction(TS ts, PetscReal ftime,Vec U, Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      hx,hy;
  PetscScalar    **uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;
  PetscReal      coef,expo,t;
  PetscReal      x,y;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  ierr = TSGetTime(ts,&t);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_SELF," current time is %g ",t);
  expo = -8.0 * c * PETSC_PI * PETSC_PI * t;
  coef = -8.0 * c * PETSC_PI * PETSC_PI * PetscExpReal(expo);
  for (j=ys; j<ys+ym; j++) {
    y = j * hy;
    for (i=xs; i<xs+xm; i++) {
      x = i * hx;
      f[j][i] = coef * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(J,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


PetscErrorCode TrueSolution(TS ts,PetscReal t,Vec U,AppCtx *ptr)
{
  	PetscReal      c=ptr->c;
  	PetscErrorCode ierr;
	DM					da;
  	PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  	PetscScalar    **u;
  	PetscReal      hx,hy,x,y,expo;

  	PetscFunctionBeginUser;
	ierr = TSGetDM(ts, &da);
  	ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  	hx = 1.0/(PetscReal)Mx;
  	hy = 1.0/(PetscReal)My;
   expo = -8.0 * c * PETSC_PI * PETSC_PI *t;

  	/* Get pointers to vector data */
  	ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  	/* Get local grid boundaries */
  	ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  	/* Compute function over the locally owned part of the grid */
  	for (j=ys; j<ys+ym; j++) {
    	y = j*hy;
    	for (i=xs; i<xs+xm; i++) {
      	x = i*hx;
      	u[j][i] = PetscExpReal(expo) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    	}
  	}

  	/* Restore vectors */
  	ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  	PetscFunctionReturn(0);
}

PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime, Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscReal      norm_test,dt,t;
  //PetscViewer    viewer;
  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);

	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"\tCache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{ 
    ierr = RHSFunction(ts,t,appctx->u_pre,appctx->rhs_dis,appctx);CHKERRQ(ierr);
    ierr = RHSJacobian(ts,t,appctx->u_pre,appctx->J_aux,appctx->J_aux2,appctx);CHKERRQ(ierr);
    // view the data
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"rhs_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = VecView(appctx->rhs_dis, viewer);
    // ierr = PetscViewerDestroy(&viewer);
    
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"jac_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = MatView(appctx->J_aux,viewer);
    // ierr = PetscViewerDestroy(&viewer);

    ierr = MatMult(appctx->J_aux,appctx->rhs_dis,appctx->r_1); // F' * F
    ierr = MatMult(appctx->J_aux,appctx->r_1,appctx->r[step]); // F' * F' * F
    // scale the vector. there is one parameter unkown.
    ierr = VecScale(appctx->r[step],-dt*dt/6);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"r_%D norm is %g\n",step,norm_test);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
	}
	return 0;
}

PetscErrorCode GetRandomBasis(PetscInt bt, PetscInt k, Vec *z, Vec *zorth, PetscRandom rnd)
{
	PetscErrorCode				ierr;
	PetscInt 					i,j,r,nrow;
	PetscViewer					viewer;
	PetscReal					norm_test;
	Mat							basis;
	Vec							z_copy;
	PetscScalar					val;


	ierr = VecSetRandom(z[0],rnd);CHKERRQ(ierr);
	ierr = VecNorm(z[0],NORM_2,&norm_test);CHKERRQ(ierr);
	ierr = VecScale(z[0],1./norm_test);CHKERRQ(ierr);
	ierr = VecCopy(z[0],zorth[0]);CHKERRQ(ierr);

	ierr = VecDuplicate(z[0],&z_copy);
	ierr = VecGetSize(zorth[0],&nrow);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&basis);CHKERRQ(ierr);
	ierr = MatSetSizes(basis,PETSC_DECIDE,PETSC_DECIDE,nrow,k);CHKERRQ(ierr);
	ierr = MatSetType(basis,MATAIJ);CHKERRQ(ierr);
	ierr = MatSetUp(basis);CHKERRQ(ierr);

	i = 0;
	for (r = 0; r < nrow; r++)
	{
		ierr = VecGetValues(zorth[0],1,&r,&val);
		ierr = MatSetValues(basis,1,&r,1,&i,&val,INSERT_VALUES);CHKERRQ(ierr);
	}

	if (bt == 1)
	{
		for (i = 1; i < k; i++)
		{
			ierr = VecSetRandom(z[i],rnd);CHKERRQ(ierr);
			ierr = VecCopy(z[i],z_copy);CHKERRQ(ierr);
			for (j = 0; j < i; j++)
			{
				ierr = VecDot(z_copy,zorth[j],&norm_test);CHKERRQ(ierr);
				ierr = VecAXPY(z[i],-norm_test,zorth[j]);CHKERRQ(ierr);
			}
			ierr = VecNorm(z[i],NORM_2,&norm_test);CHKERRQ(ierr);
			ierr = VecScale(z[i],1./norm_test);CHKERRQ(ierr);
			ierr = VecCopy(z[i],zorth[i]);CHKERRQ(ierr);
			
			for (r = 0; r < nrow; r++)
			{
				ierr = VecGetValues(zorth[i],1,&r,&val);
				ierr = MatSetValues(basis,1,&r,1,&i,&val,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	MatAssemblyBegin(basis,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(basis,MAT_FINAL_ASSEMBLY);

	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"heat_2d_orth_basis.dat",FILE_MODE_WRITE,&viewer);
	MatView(basis, viewer);
	PetscViewerDestroy(&viewer);


	// PetscPrintf(PETSC_COMM_SELF,"Check orhogonality: \n");
	// VecDot(zorth[0], zorth[3],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[1], zorth[2],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[3], zorth[4],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[0], zorth[4],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// VecDot(zorth[2], zorth[3],&norm_test);
	// PetscPrintf(PETSC_COMM_SELF,"%D dot %D  = %g\n", 0,3,norm_test);
	// PetscFree(row_index);
	VecDestroy(&z_copy);
	return 0;
}

PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscReal      dt, norm1, norm2;

	ierr = TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += (norm1 + norm2)/2.0 * appctx->dt_pre;
    // PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	return ierr;
}