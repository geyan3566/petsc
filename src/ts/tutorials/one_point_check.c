
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = uxx + uyy
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125

    mpiexec -n 2 ./ex13 -da_grid_x 40 -da_grid_y 40 -ts_max_steps 2 -snes_monitor -ksp_monitor
    mpiexec -n 1 ./ex13 -snes_fd_color -ts_monitor_draw_solution
    mpiexec -n 2 ./ex13 -ts_type sundials -ts_monitor
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
  PetscReal c;
	Vec       adj_pre, lambda[1];
	PetscReal computed_error_norm, estimated_error_norm, integrated_error_norm,initial_error_norm;
	PetscReal dt_pre;
  PetscInt  actual_steps;
} AppCtx;

extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode FormInitialSolution(DM,Vec,void*);
extern PetscErrorCode IFunction(TS, PetscReal, Vec, Vec, Vec, void*);
extern PetscErrorCode IJacobian(TS, PetscReal, Vec, Vec, PetscReal, Mat, Mat, void*);
extern PetscErrorCode my_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  Vec            u,r, solution;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscInt       steps;                /* iterations for convergence */
  PetscErrorCode ierr;
  DM             da;
  PetscReal      ftime,dt,dx, simu_error_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscInt       m = 20;
  PetscReal      dt_coef = 0.5;
  PetscInt       loc = 0;
  PetscScalar    one = 1.0;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-l",&loc,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create distributed array (DMDA) to manage parallel grid and vectors
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);

  /*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Extract global vectors from DMDA;
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(da,&solution);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);

  /* Initialize user application context */
  user.c = 0.025;
	user.computed_error_norm = 0.0;
	user.estimated_error_norm = 0.0;
	user.integrated_error_norm = 0.0;
	user.initial_error_norm = 0.0;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create timestepping solver context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);

  /* Set Jacobian */
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);

  ftime = 0.5;
  ierr = TSSetMaxTime(ts,ftime);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_INTERPOLATE);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set initial conditions
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
	dx   = 1.0/m;
  dt   = dt_coef * dx * dx;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
  /*VecView(u, PETSC_VIEWER_STDOUT_WORLD);*/
	

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set runtime options
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Solve nonlinear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);

  /* save the results and check */
	
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);

	/* Compute the solution error*/
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"Final time is %g, and final step is %D.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     The adjoint section start from here
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  VecDuplicate(u, &user.adj_pre);CHKERRQ(ierr);
	{
  PetscRandom rnd;
	PetscReal norm_initial;
	PetscReal norm_solution;
	PetscReal cond, norm_pert;
	PetscReal en = 1.0;
	Vec initial_pert;


	ierr = VecDuplicate(u,&user.lambda[0]);CHKERRQ(ierr);
	ierr = VecZeroEntries(user.lambda[0]); CHKERRQ(ierr);
  ierr = VecSetValues(user.lambda[0], 1, &loc, &one, INSERT_VALUES);CHKERRQ(ierr);

	ierr = VecNorm(user.lambda[0], NORM_2, &norm_initial); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Initial norm is (should be 1) %g\n ", norm_initial);

	ierr = TSSetCostGradients(ts,1,user.lambda,NULL);CHKERRQ(ierr);
	ierr = TSAdjointMonitorSet(ts, my_monitor, &user, NULL); CHKERRQ(ierr);
  ierr = TSAdjointSolve(ts);CHKERRQ(ierr);

	ierr = VecNorm(user.lambda[0], NORM_2, &norm_solution); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Adjoint solution norm is %g \n", norm_solution);
	cond = norm_initial + norm_solution;
	ierr = PetscPrintf(PETSC_COMM_SELF, "Condition number is %g \n", cond);
	if (m % 2 == 0)
	{
		en *= 2.0 / PETSC_PI;
		for (int i = 1; i <= m/2 - 1; i++)
		{
			en *= ((2. * i) / (2. * i - 1.));
		}
	}
	else
	{
			for (int i = 1; i <= (m-1)/2; i++)
			{
				en *= ((2. * i - 1.) / (2. * i));
			}
	}

	ierr = PetscPrintf(PETSC_COMM_SELF, "Coefficient E_n is %g \n", en);
	user.estimated_error_norm = cond / en * 0.002;
	ierr = PetscPrintf(PETSC_COMM_SELF, "Estimated error is %g \n", user.estimated_error_norm);
	ierr = VecDuplicate(u,&initial_pert); CHKERRQ(ierr);
  PetscRandomCreate(PETSC_COMM_WORLD, &rnd);
	PetscRandomSetInterval(rnd, -1.0, 1.0);
	PetscRandomSetFromOptions(rnd);
	VecSetRandom(initial_pert, rnd);
  VecNorm(initial_pert, NORM_2, &norm_pert);
  ierr = PetscPrintf(PETSC_COMM_SELF, "Norm of psyduo perturbation is %g, ", norm_pert);
  ierr = VecScale(initial_pert, 1e-13); CHKERRQ(ierr);
  ierr = VecNorm(initial_pert, NORM_2, &norm_pert);
  ierr = PetscPrintf(PETSC_COMM_SELF, "after scaling the norm is %g \n", norm_pert);

	VecDot(user.lambda[0], initial_pert, &user.initial_error_norm);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Initial induced error is %g \n", user.initial_error_norm);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Integrated error is %g \n", user.integrated_error_norm);
 	user.computed_error_norm = user.integrated_error_norm + user.initial_error_norm;
  ierr = PetscPrintf(PETSC_COMM_SELF, "Computed error is %g \n", user.computed_error_norm);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Estimated error is %g \n", user.estimated_error_norm);
	ierr = PetscRandomDestroy(&rnd);
	ierr = VecDestroy(&initial_pert);CHKERRQ(ierr);
	}



  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = VecDestroy(&user.lambda[0]);CHKERRQ(ierr);
  ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode my_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
	PetscReal      dt, norm1, norm2;
	PetscInt       max_step;
	Vec            r;

	ierr = VecDuplicate(lam[0],&r);CHKERRQ(ierr);

	ierr = TSGetMaxSteps(ts, &max_step);
  PetscPrintf(PETSC_COMM_SELF, "The ODE has run steps: %D\n", max_step);
	ierr = TSGetTimeStep(ts, &dt);
	VecSet(r, 5.0 * dt * dt);
	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
		PetscPrintf(PETSC_COMM_SELF, "Cach initial states.\n");
	}
	else
	{
		VecDot(appctx->adj_pre, r, &norm1);
		VecDot(lam[0], r, &norm2);
		appctx->integrated_error_norm = -(norm1 + norm2)/2.0 * appctx->dt_pre;
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
  VecDestroy(&r);CHKERRQ(ierr);
	return 0;
}

/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */
PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   U - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   J - Jacobian matrix
   Jpre - optionally different preconditioning matrix
   str - flag indicating matrix structure
*/
PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode FormInitialSolution(DM da,Vec U,void* ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscReal      c=user->c;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;
	c  += c;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y, c;
  c = user->c;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscExpReal(-8.0 * PETSC_PI * PETSC_PI *ftime *c) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
