static char help[] = "Demonstrates adjoint sensitivity analysis for Reaction-Diffusion Equations.\n";

/*
  See ex5.c for details on the equation.
  This code demonestrates the TSAdjoint interface to a system of time-dependent partial differential equations.
  It computes the sensitivity of a component in the final solution, which locates in the center of the 2D domain, w.r.t. the initial conditions.
  The user does not need to provide any additional functions. The required functions in the original simulation are reused in the adjoint run.

  Runtime options:
    -forwardonly  - run the forward simulation without adjoint
    -implicitform - provide IFunction and IJacobian to TS, if not set, RHSFunction and RHSJacobian will be used
    -aijpc        - set the preconditioner matrix to be aij (the Jacobian matrix can be of a different type such as ELL)
*/

#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

typedef struct {
  PetscScalar u,v;
} Field;

typedef struct {
  PetscReal D1,D2,gamma,kappa;
  PetscBool aijpc;

  // variables for ts monitor
  Vec u_pre, rhs_dis, r_1;
  Vec *r;
  Mat J_aux, J_aux2;

  // variables for ts adj monitor
  Vec adj_pre;
  Vec lambda[1];
  PetscInt actual_steps;
  PetscReal dt_pre;
  PetscReal integrated_error_norm;
} AppCtx;

/*
   User-defined routines
*/
PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
PetscErrorCode InitialConditions(DM,Vec);
PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);

// monitors
PetscErrorCode ts_monitor(TS,PetscInt,PetscReal,Vec,void*);
PetscErrorCode ts_adj_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);

PetscErrorCode InitializeLambda(DM da,Vec lambda,PetscReal x,PetscReal y)
{
   PetscInt i,j,Mx,My,xs,ys,xm,ym;
   PetscErrorCode ierr;
   Field **l;
   PetscFunctionBegin;

   ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
   /* locate the global i index for x and j index for y */
   i = (PetscInt)(x*(Mx-1));
   j = (PetscInt)(y*(My-1));
   ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

   if (xs <= i && i < xs+xm && ys <= j && j < ys+ym) {
     /* the i,j vertex is on this process */
     ierr = DMDAVecGetArray(da,lambda,&l);CHKERRQ(ierr);
     l[j][i].u = 1.0;
     l[j][i].v = 1.0;
     ierr = DMDAVecRestoreArray(da,lambda,&l);CHKERRQ(ierr);
   }
   PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  TS             ts;                  /* ODE integrator */
  Vec            x;                   /* solution */
  // Vec            lambda[1];
  PetscErrorCode ierr;
  DM             da;
  AppCtx         appctx;
  PetscBool      forwardonly=PETSC_FALSE;
  PetscInt       steps = 0;
  PetscReal      ftime = 0.0;

  // user induced variables
  PetscViewer     viewer;
  Vec             r;
  Vec             error_distribution;
  Mat             J;
  PetscInt        max_steps = 100;
  PetscInt        s = 20;
  PetscScalar     one = 1.0;
  PetscReal       norm_test = 0.0;
  PetscReal       dt = 0.5;


  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
  ierr = PetscOptionsGetBool(NULL,NULL,"-forwardonly",&forwardonly,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ms",&max_steps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-s",&s,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);CHKERRQ(ierr);

  appctx.aijpc = PETSC_FALSE;
  appctx.D1    = 8.0e-5;
  appctx.D2    = 4.0e-5;
  appctx.gamma = .024;
  appctx.kappa = .06;
  appctx.integrated_error_norm = 0.0;
  appctx.dt_pre = 0.0;
  appctx.actual_steps = 0;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create distributed array (DMDA) to manage parallel grid and vectors
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_PERIODIC,DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,s,s,PETSC_DECIDE,PETSC_DECIDE,2,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,0,"u");CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,1,"v");CHKERRQ(ierr);

  /*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Extract global vectors from DMDA; then duplicate for remaining
     vectors that are the same types
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = DMCreateGlobalVector(da,&x);CHKERRQ(ierr);

  // initialize some induced variables
  ierr = VecDuplicate(x,&r);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&appctx.u_pre);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&appctx.rhs_dis);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&appctx.r_1);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&appctx.adj_pre);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&appctx.lambda[0]);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&error_distribution);CHKERRQ(ierr);
  max_steps = 50.0/dt;
  ierr = VecDuplicateVecs(x,max_steps+1,&appctx.r);


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create timestepping solver context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetProblemType(ts,TS_NONLINEAR);CHKERRQ(ierr);
  //ierr = TSSetEquationType(ts,TS_EQ_ODE_EXPLICIT);CHKERRQ(ierr); /* less Jacobian evaluations when adjoint BEuler is used, otherwise no effect */
  ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&appctx);CHKERRQ(ierr);

  // set jacobian
  ierr = DMSetMatType(da,MATAIJ);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&appctx.J_aux);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&appctx.J_aux2);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian,&appctx);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set initial conditions
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = InitialConditions(da,x);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,x);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"nonlinear2d_initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(x,viewer);
	PetscViewerDestroy(&viewer);


  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set solver options
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
	ierr = TSMonitorSet(ts,ts_monitor,&appctx,NULL);
  ierr = TSSetMaxTime(ts,50.0);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(ts,1e9);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Solve ODE system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TSSolve(ts,x);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"ODE solved to final time =  %gs with final steps =  %D.\n", ftime, steps);

  appctx.actual_steps = steps;
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"nonlinear2d_result.dat",FILE_MODE_WRITE,&viewer);
	VecView(x,viewer);
	PetscViewerDestroy(&viewer);



  ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\n Perform the exhausitve loop to estimate the error estimation\n\n");
  ierr = TSSetCostGradients(ts,1,appctx.lambda,NULL);CHKERRQ(ierr);
  ierr = TSAdjointMonitorSet(ts, ts_adj_monitor, &appctx, NULL);CHKERRQ(ierr);
  ierr = TSMonitorCancel(ts);CHKERRQ(ierr);

  for (int index = 0; index < 2*s*s; index++)
  {
		ierr = PetscPrintf(PETSC_COMM_WORLD, "checking %Dth error...\n", index);CHKERRQ(ierr);
    // initialize the adjoint variable
		ierr = VecZeroEntries(appctx.lambda[0]);CHKERRQ(ierr);
    ierr = VecSetValues(appctx.lambda[0], 1, &index, &one, INSERT_VALUES);CHKERRQ(ierr);
    //ierr = InitializeLambda(da,appctx.lambda[0],0.5,0.5);CHKERRQ(ierr);
    ierr = VecNorm(appctx.lambda[0], NORM_2, &norm_test);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitial norm (should be 1) is %g", norm_test);

    // solve adjoint problem
    ierr = TSAdjointSolve(ts);CHKERRQ(ierr);


		// // solve the adjoint equation
		ierr = VecSetValues(error_distribution,1,&index, &appctx.integrated_error_norm,INSERT_VALUES);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, ", integrated error norm is %g\n",appctx.integrated_error_norm);
		appctx.integrated_error_norm = 0.;

    // reperform the ts solve
  	ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&appctx);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &appctx);CHKERRQ(ierr);
	  ierr = TSSetMaxSteps(ts,max_steps);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
    ierr = InitialConditions(da,x);CHKERRQ(ierr);
    ierr = TSSetSolution(ts,x);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,x);CHKERRQ(ierr);
  }

	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"nonlinear2d_estimation.dat",FILE_MODE_WRITE,&viewer);
	VecView(error_distribution,viewer);
	PetscViewerDestroy(&viewer);
  // if (!forwardonly) {
  //   /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  //      Start the Adjoint model
  //      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  //   /*   Reset initial conditions for the adjoint integration */
  //   ierr = InitializeLambda(da,appctx.lambda[0],0.5,0.5);CHKERRQ(ierr);
  //   ierr = TSSetCostGradients(ts,1,appctx.lambda,NULL);CHKERRQ(ierr);
  //   ierr = TSAdjointSolve(ts);CHKERRQ(ierr);
  // }
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  // free induced variables
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);

  // free induced variables
  ierr = VecDestroy(&appctx.rhs_dis);CHKERRQ(ierr);
  ierr = VecDestroy(&appctx.r_1);CHKERRQ(ierr);
  ierr = VecDestroy(&appctx.u_pre);CHKERRQ(ierr);
  ierr = VecDestroy(&appctx.adj_pre);CHKERRQ(ierr);
  ierr = VecDestroy(&appctx.lambda[0]);CHKERRQ(ierr);
  ierr = VecDestroy(&error_distribution);CHKERRQ(ierr);
  ierr = VecDestroyVecs(max_steps+1,&appctx.r);CHKERRQ(ierr);
  ierr = MatDestroy(&appctx.J_aux);CHKERRQ(ierr);
  ierr = MatDestroy(&appctx.J_aux2);CHKERRQ(ierr);


  ierr = PetscFinalize();
  return ierr;
}

/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(x).

   Input Parameters:
.  ts - the TS context
.  X - input vector
.  ptr - optional user-defined context, as set by TSSetRHSFunction()

   Output Parameter:
.  F - function vector
 */
PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *appctx = (AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      hx,hy,sx,sy;
  PetscScalar    uc,uxx,uyy,vc,vxx,vyy;
  Field          **u,**f;
  Vec            localU;

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  hx = 2.50/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 2.50/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /*
     Get pointers to vector data
  */
  ierr = DMDAVecGetArrayRead(da,localU,&u);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /*
     Get local grid boundaries
  */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /*
     Compute function over the locally owned part of the grid
  */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      uc        = u[j][i].u;
      uxx       = (-2.0*uc + u[j][i-1].u + u[j][i+1].u)*sx;
      uyy       = (-2.0*uc + u[j-1][i].u + u[j+1][i].u)*sy;
      vc        = u[j][i].v;
      vxx       = (-2.0*vc + u[j][i-1].v + u[j][i+1].v)*sx;
      vyy       = (-2.0*vc + u[j-1][i].v + u[j+1][i].v)*sy;
      f[j][i].u = appctx->D1*(uxx + uyy) - uc*vc*vc + appctx->gamma*(1.0 - uc);
      f[j][i].v = appctx->D2*(vxx + vyy) + uc*vc*vc - (appctx->gamma + appctx->kappa)*vc;
    }
  }
  ierr = PetscLogFlops(16.0*xm*ym);CHKERRQ(ierr);

  /*
     Restore vectors
  */
  ierr = DMDAVecRestoreArrayRead(da,localU,&u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode InitialConditions(DM da,Vec U)
{
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  Field          **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBegin;
  PetscCall(DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE));

  hx = 2.5/(PetscReal)Mx;
  hy = 2.5/(PetscReal)My;

  /*
     Get pointers to vector data
  */
  PetscCall(DMDAVecGetArray(da,U,&u));

  /*
     Get local grid boundaries
  */
  PetscCall(DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL));

  /*
     Compute function over the locally owned part of the grid
  */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      if (PetscApproximateGTE(x,1.0) && PetscApproximateLTE(x,1.5) && PetscApproximateGTE(y,1.0) && PetscApproximateLTE(y,1.5)) u[j][i].v = PetscPowReal(PetscSinReal(4.0*PETSC_PI*x),2.0)*PetscPowReal(PetscSinReal(4.0*PETSC_PI*y),2.0)/4.0;
      else u[j][i].v = 0.0;

      u[j][i].u = 1.0 - 2.0*u[j][i].v;
    }
  }

  /*
     Restore vectors
  */
  PetscCall(DMDAVecRestoreArray(da,U,&u));
  PetscFunctionReturn(0);
}

PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat A,Mat BB,void *ctx)
{
  AppCtx         *appctx = (AppCtx*)ctx;     /* user-defined application context */
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      hx,hy,sx,sy;
  PetscScalar    uc,vc;
  Field          **u;
  Vec            localU;
  MatStencil     stencil[6],rowstencil;
  PetscScalar    entries[6];

  PetscFunctionBegin;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 2.50/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 2.50/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /*
     Get pointers to vector data
  */
  ierr = DMDAVecGetArrayRead(da,localU,&u);CHKERRQ(ierr);

  /*
     Get local grid boundaries
  */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  stencil[0].k = 0;
  stencil[1].k = 0;
  stencil[2].k = 0;
  stencil[3].k = 0;
  stencil[4].k = 0;
  stencil[5].k = 0;
  rowstencil.k = 0;
  /*
     Compute function over the locally owned part of the grid
  */
  for (j=ys; j<ys+ym; j++) {

    stencil[0].j = j-1;
    stencil[1].j = j+1;
    stencil[2].j = j;
    stencil[3].j = j;
    stencil[4].j = j;
    stencil[5].j = j;
    rowstencil.k = 0; rowstencil.j = j;
    for (i=xs; i<xs+xm; i++) {
      uc = u[j][i].u;
      vc = u[j][i].v;

      /*      uxx       = (-2.0*uc + u[j][i-1].u + u[j][i+1].u)*sx;
      uyy       = (-2.0*uc + u[j-1][i].u + u[j+1][i].u)*sy;

      vxx       = (-2.0*vc + u[j][i-1].v + u[j][i+1].v)*sx;
      vyy       = (-2.0*vc + u[j-1][i].v + u[j+1][i].v)*sy;
       f[j][i].u = appctx->D1*(uxx + uyy) - uc*vc*vc + appctx->gamma*(1.0 - uc);*/

      stencil[0].i = i; stencil[0].c = 0; entries[0] = appctx->D1*sy;
      stencil[1].i = i; stencil[1].c = 0; entries[1] = appctx->D1*sy;
      stencil[2].i = i-1; stencil[2].c = 0; entries[2] = appctx->D1*sx;
      stencil[3].i = i+1; stencil[3].c = 0; entries[3] = appctx->D1*sx;
      stencil[4].i = i; stencil[4].c = 0; entries[4] = -2.0*appctx->D1*(sx + sy) - vc*vc - appctx->gamma;
      stencil[5].i = i; stencil[5].c = 1; entries[5] = -2.0*uc*vc;
      rowstencil.i = i; rowstencil.c = 0;

      ierr = MatSetValuesStencil(A,1,&rowstencil,6,stencil,entries,INSERT_VALUES);CHKERRQ(ierr);
      if (appctx->aijpc) {
        ierr = MatSetValuesStencil(BB,1,&rowstencil,6,stencil,entries,INSERT_VALUES);CHKERRQ(ierr);
      }
      stencil[0].c = 1; entries[0] = appctx->D2*sy;
      stencil[1].c = 1; entries[1] = appctx->D2*sy;
      stencil[2].c = 1; entries[2] = appctx->D2*sx;
      stencil[3].c = 1; entries[3] = appctx->D2*sx;
      stencil[4].c = 1; entries[4] = -2.0*appctx->D2*(sx + sy) + 2.0*uc*vc - appctx->gamma - appctx->kappa;
      stencil[5].c = 0; entries[5] = vc*vc;
      rowstencil.c = 1;

      ierr = MatSetValuesStencil(A,1,&rowstencil,6,stencil,entries,INSERT_VALUES);CHKERRQ(ierr);
      if (appctx->aijpc) {
        ierr = MatSetValuesStencil(BB,1,&rowstencil,6,stencil,entries,INSERT_VALUES);CHKERRQ(ierr);
      }
      /* f[j][i].v = appctx->D2*(vxx + vyy) + uc*vc*vc - (appctx->gamma + appctx->kappa)*vc; */
    }
  }

  /*
     Restore vectors
  */
  ierr = PetscLogFlops(19.0*xm*ym);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(da,localU,&u);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatSetOption(A,MAT_NEW_NONZERO_LOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
  if (appctx->aijpc) {
    ierr = MatAssemblyBegin(BB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(BB,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatSetOption(BB,MAT_NEW_NONZERO_LOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}


// ts_monitro
PetscErrorCode ts_monitor(TS ts, PetscInt step, PetscReal ptime,Vec u, void* ctx)
{
	AppCtx         *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
  PetscReal      norm_test,dt,t;
  //PetscViewer    viewer;
  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);

  ierr = TSGetTimeStep(ts, &dt);
  ierr = TSGetTime(ts,&t);
	if (0 == step)
	{
    ierr = VecZeroEntries(appctx->r[step]);CHKERRQ(ierr);
		ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_SELF,"Cache u_0, r_0 norm is %g\n",norm_test);
	}
	else
	{ 
    ierr = RHSFunction(ts,t,appctx->u_pre,appctx->rhs_dis,appctx);CHKERRQ(ierr);
    ierr = RHSJacobian(ts,t,appctx->u_pre,appctx->J_aux,appctx->J_aux2,appctx);CHKERRQ(ierr);
    // view the data
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"rhs_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = VecView(appctx->rhs_dis, viewer);
    // ierr = PetscViewerDestroy(&viewer);
    
    // ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"jac_example2.dat",FILE_MODE_WRITE,&viewer);
    // ierr = MatView(appctx->J_aux,viewer);
    // ierr = PetscViewerDestroy(&viewer);

    ierr = MatMult(appctx->J_aux,appctx->rhs_dis,appctx->r_1); // F' * F
    ierr = MatMult(appctx->J_aux,appctx->r_1,appctx->r[step]); // F' * F' * F
    // scale the vector. there is one parameter unkown.
    ierr = VecScale(appctx->r[step],-dt*dt/6);CHKERRQ(ierr);
    ierr = VecNorm(appctx->r[step],NORM_2,&norm_test);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"dt =%g, r_%D norm is %g\n",dt,step,norm_test);
    ierr = VecCopy(u,appctx->u_pre);CHKERRQ(ierr);
	}
	PetscFunctionReturn(0);
}


PetscErrorCode ts_adj_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscReal      dt, norm1, norm2;

	TSGetTimeStep(ts, &dt);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->r[step+1], &norm1);
		VecDot(lam[0], appctx->r[step], &norm2);
		appctx->integrated_error_norm += (norm1 + norm2)/2.0 * appctx->dt_pre;
    //PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0], appctx->adj_pre);
		appctx->dt_pre = dt;
	}
	PetscFunctionReturn(0);
}