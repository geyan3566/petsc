
static char help[] = "Time-dependent PDE in 2d. Simplified from ex7.c for illustrating how to use TS on a structured domain. \n";
/*
   u_t = uxx + uyy
   0 < x < 1, 0 < y < 1;
   At t=0: u(x,y) = exp(c*r*r*r), if r=PetscSqrtReal((x-.5)*(x-.5) + (y-.5)*(y-.5)) < .125
           u(x,y) = 0.0           if r >= .125

    mpiexec -n 2 ./ex13 -da_grid_x 40 -da_grid_y 40 -ts_max_steps 2 -snes_monitor -ksp_monitor
    mpiexec -n 1 ./ex13 -snes_fd_color -ts_monitor_draw_solution
    mpiexec -n 2 ./ex13 -ts_type sundials -ts_monitor
*/

#include <petscdm.h>
#include <petscdmda.h>
#include <petscts.h>

/*
   User-defined data structures and routines
*/
typedef struct {
  PetscReal c;
	Vec       adj_pre, lambda[1], error_pre;
	PetscReal computed_error_norm, estimated_error_norm, integrated_error_norm,initial_error_norm;
	PetscReal dt_pre, coef;
  PetscInt  actual_steps;

  // data for computing the error
  TS        ts_fine;
  Mat       J_fine;
  Vec       r_fine,u_fine;
  PetscInt  steps_fine;
} AppCtx;

extern PetscErrorCode RHSFunction(TS,PetscReal,Vec,Vec,void*);
extern PetscErrorCode RHSJacobian(TS,PetscReal,Vec,Mat,Mat,void*);
extern PetscErrorCode FormInitialSolution(DM,Vec,void*);
extern PetscErrorCode IFunction(TS, PetscReal, Vec, Vec, Vec, void*);
extern PetscErrorCode IJacobian(TS, PetscReal, Vec, Vec, PetscReal, Mat, Mat, void*);
extern PetscErrorCode my_monitor(TS, PetscInt, PetscReal, Vec, PetscInt, Vec*, Vec*, void*);
extern PetscErrorCode ExactSolution(DM, Vec, void*, PetscReal);
extern PetscErrorCode ComputeEn(PetscInt, PetscReal*);

int main(int argc,char **argv)
{
  TS             ts;                   /* nonlinear solver */
  DM             da, da_fine;
  Vec            u,r,solution;        /* solution, residual vector */
  Mat            J;                    /* Jacobian matrix */
  PetscInt       steps;                /* iterations for convergence */
  PetscErrorCode ierr;
  PetscReal      ftime,dt,dx, simu_error_norm;
  AppCtx         user;              /* user-defined work context */
	PetscViewer    viewer;
	PetscInt       m = 20;
	PetscScalar    one = 1.0;
  PetscReal      dt_coef = 0.5;
	PetscReal      coef = 5.0;
	PetscReal 		 cond, norm_initial, norm_solution, En=1.0;
	Vec            error_distribution;
  PetscInt       steps_fine = 4;
  


  ierr = PetscInitialize(&argc,&argv,(char*)0,help);if (ierr) return ierr;
	ierr = PetscOptionsGetInt(NULL,NULL,"-m",&m,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&dt_coef,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(NULL,NULL,"-coef",&coef,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-sf",&steps_fine,NULL);CHKERRQ(ierr);

	// initialize some parameters
	user.c = 0.025;
	user.coef = coef;
  user.steps_fine = steps_fine;
	user.computed_error_norm = 0.0;
	user.estimated_error_norm = 0.0;
	user.integrated_error_norm = 0.0;
	user.initial_error_norm = 0.0;

	// create the discretization
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_PERIODIC, DM_BOUNDARY_PERIODIC,DMDA_STENCIL_STAR,m,m,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da_fine);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMSetFromOptions(da_fine);CHKERRQ(ierr);
  ierr = DMSetUp(da);CHKERRQ(ierr);
  ierr = DMSetUp(da_fine);CHKERRQ(ierr);


	// create the global vector
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(da,&solution);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da_fine,&user.u_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&r);CHKERRQ(ierr);
  ierr = VecDuplicate(user.u_fine,&user.r_fine);CHKERRQ(ierr);

	// create the time steping solver
  ierr = TSCreate(PETSC_COMM_WORLD,&ts);CHKERRQ(ierr);
  ierr = TSCreate(PETSC_COMM_WORLD,&user.ts_fine);CHKERRQ(ierr);
  ierr = TSSetDM(ts,da);CHKERRQ(ierr);
  ierr = TSSetDM(user.ts_fine,da_fine);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(user.ts_fine,user.r_fine,RHSFunction,&user);CHKERRQ(ierr);

  // set jacobian
  ierr = DMSetMatType(da,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = DMCreateMatrix(da_fine,&user.J_fine);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
  ierr = TSSetRHSJacobian(user.ts_fine,user.J_fine,user.J_fine,RHSJacobian,&user);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetType(user.ts_fine,TSCN);CHKERRQ(ierr);

  // set the final time
  ftime = .8;
  ierr = TSSetMaxTime(ts,ftime);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
	ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);

  // set the time step size  
	dx   = 1.0/m;
  dt   =  dt_coef * dx * dx / user.c;
  ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);

  // save the initial condition
  ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"initial.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);

	
  // set from option 
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  // solve the linear system and
  ierr = TSSolve(ts,u);CHKERRQ(ierr);
  ierr = TSGetSolveTime(ts,&ftime);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
  user.actual_steps= steps;
	

  // save the results and exact solution
	ierr = ExactSolution(da, solution, &user, ftime); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"Writing simulation results to results.dat ...\n");
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"result.dat",FILE_MODE_WRITE,&viewer);
	VecView(u, viewer);
	PetscViewerDestroy(&viewer);
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"solution.dat",FILE_MODE_WRITE,&viewer);
	VecView(solution, viewer);
	PetscViewerDestroy(&viewer);
  PetscPrintf(PETSC_COMM_SELF,"The simulation results is:\n");
  VecView(u,PETSC_VIEWER_STDOUT_WORLD);
  PetscPrintf(PETSC_COMM_WORLD,"Exact solution is:\n");
  VecView(solution,PETSC_VIEWER_STDOUT_WORLD);

	// Compute simulation error
	ierr = VecAXPY(solution, -1.0, u); CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"error vector is:\n");
  VecView(solution,PETSC_VIEWER_STDOUT_WORLD);
	ierr = VecNorm(solution, NORM_2, &simu_error_norm); CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_SELF,"Final time is %g, and final step is %D.\n", ftime, steps);
	ierr = PetscPrintf(PETSC_COMM_SELF, "Solution error norm is %g \n", simu_error_norm);

	// perform the exhaustive loop to compute the estimated error
	ierr = VecDuplicate(u, &user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDuplicate(u, &user.adj_pre);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &user.error_pre);CHKERRQ(ierr);
	ierr = VecDuplicate(u, &error_distribution);CHKERRQ(ierr);
	ierr = VecZeroEntries(error_distribution);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD, "\n\nPerform the exhaustive loop error estimation.\n\n");
	ierr = ComputeEn(m,&En);CHKERRQ(ierr);
  ierr = TSSetCostGradients(ts,1,user.lambda,NULL);CHKERRQ(ierr);
  ierr = TSAdjointMonitorSet(ts, my_monitor, &user, NULL);CHKERRQ(ierr);
	for (int index = 0; index < m*m; index++)
	{		
		/* Adjoint section from here */
		ierr = PetscPrintf(PETSC_COMM_WORLD, "cheking %D th error:\n", index);CHKERRQ(ierr);
		
		ierr = VecZeroEntries(user.lambda[0]);CHKERRQ(ierr);
    ierr = VecSetValues(user.lambda[0], 1, &index, &one, INSERT_VALUES);CHKERRQ(ierr);

    ierr = VecNorm(user.lambda[0], NORM_2, &norm_initial);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\tInitial norm (should be 1) is %g\n", norm_initial);
    ierr = TSAdjointSolve(ts);CHKERRQ(ierr);

		ierr = VecNorm(user.lambda[0], NORM_2, &norm_solution);CHKERRQ(ierr);
	  ierr = PetscPrintf(PETSC_COMM_SELF, "\tAdjoint solution norm is %g \n", norm_solution);
	  cond = norm_initial + norm_solution;
	  ierr = PetscPrintf(PETSC_COMM_SELF, "\tCondition number is %g \n", cond);
		ierr = PetscPrintf(PETSC_COMM_SELF, "\tCoefficient E_n is %g \n", En);
		user.estimated_error_norm = cond / En * 0.002;
		ierr = PetscPrintf(PETSC_COMM_SELF, "\tEstimated error is %g \n", user.estimated_error_norm);
		ierr = PetscPrintf(PETSC_COMM_SELF, "\tIntegrated error is %g \n", user.integrated_error_norm);

		/* save the single value */
		ierr = VecSetValues(error_distribution, 1, &index, &user.integrated_error_norm, INSERT_VALUES);CHKERRQ(ierr);

		/* reset some values */
		user.estimated_error_norm = 0.;
		user.integrated_error_norm = 0.;

		/* reperform the ode simulation, because of the step issue */
		ierr = PetscPrintf(PETSC_COMM_SELF, "\tBring the adjoint solve back to initial step\n");
		ierr = TSReset(ts);CHKERRQ(ierr);
		ierr = TSSetDM(ts,da);CHKERRQ(ierr);
    ierr = TSSetRHSFunction(ts,r,RHSFunction,&user);CHKERRQ(ierr);
  	ierr = TSSetRHSJacobian(ts,J,J,RHSJacobian, &user);CHKERRQ(ierr);
	  ierr = TSSetMaxTime(ts,ftime);CHKERRQ(ierr);
  	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
		ierr = TSSetSaveTrajectory(ts);CHKERRQ(ierr);
		ierr = FormInitialSolution(da,u,&user);CHKERRQ(ierr);
		ierr = TSSetTimeStep(ts,dt);CHKERRQ(ierr);
		ierr = TSSolve(ts,u);CHKERRQ(ierr);
		/* Free up the looping memory */
	}

	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"error_distribution.dat",FILE_MODE_WRITE,&viewer);
	VecView(error_distribution, viewer);
	PetscViewerDestroy(&viewer);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Free work space.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	ierr = VecDestroy(&error_distribution);CHKERRQ(ierr);
	ierr = VecDestroy(&user.lambda[0]);CHKERRQ(ierr);
	ierr = VecDestroy(&user.adj_pre);CHKERRQ(ierr);
  ierr = VecDestroy(&user.u_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&user.r_fine);CHKERRQ(ierr);
  ierr = MatDestroy(&user.J_fine);CHKERRQ(ierr);		
	ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  ierr = DMDestroy(&da_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&solution);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

PetscErrorCode my_monitor(TS ts, PetscInt step, PetscReal crtime, Vec u, PetscInt nc, Vec *lam, Vec* mu, void* ctx)
{
	AppCtx     	   *appctx = (AppCtx*) ctx;
	PetscErrorCode ierr;
  PetscInt       step_fine;
	PetscReal      dt, dt_fine,ftime_fine,time_fine;
  PetscReal      norm1, norm2, norm_test;
	Vec            r;

  // solve the ode again with finer time step size
  ierr = TSGetTimeStep(ts, &dt);CHKERRQ(ierr);
  dt_fine = -dt/4.0;
  ierr = TSSetTimeStep(appctx->ts_fine, dt_fine);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(appctx->ts_fine,appctx->steps_fine);CHKERRQ(ierr);

  ierr = VecCopy(u,appctx->u_fine);
  ierr = TSGetTime(appctx->ts_fine,&time_fine);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"\tts_fine solve the ode up to %D steps with dt_fine = %g, current time is %g. ",appctx->steps_fine,dt_fine,time_fine);
  ierr = TSSetTime(appctx->ts_fine,0.0);CHKERRQ(ierr);
  ierr = TSSolve(appctx->ts_fine,appctx->u_fine);CHKERRQ(ierr);
  ierr = TSGetSolveTime(appctx->ts_fine,&ftime_fine);
  ierr = TSGetStepNumber(appctx->ts_fine,&step_fine);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Actual final time ftime_fine = %g and step_fine = %D \n", ftime_fine, step_fine);

  PetscPrintf(PETSC_COMM_SELF,"\tcompare two results is:\n");
  VecView(u,PETSC_VIEWER_STDOUT_WORLD);
  VecView(appctx->u_fine, PETSC_VIEWER_STDOUT_WORLD);

	ierr = VecDuplicate(appctx->u_fine, &r);CHKERRQ(ierr);
  ierr = VecCopy(appctx->u_fine, r);
  ierr = VecAXPY(r, -1.0, u);CHKERRQ(ierr);
  ierr = VecNorm(r,NORM_2,&norm_test);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF,"\tRelative error norm is %g.\n",norm_test);
  VecView(r,PETSC_VIEWER_STDOUT_WORLD);

	if (step == appctx->actual_steps)
	{
		VecCopy(lam[0], appctx->adj_pre);
    VecCopy(r,appctx->error_pre);
		appctx->dt_pre = dt;
    PetscPrintf(PETSC_COMM_SELF, "\tcache dt, adjoint, and r_13...\n");
	}
	else
	{
		VecDot(appctx->adj_pre, appctx->error_pre, &norm1);
		VecDot(lam[0], r, &norm2);
		appctx->integrated_error_norm += -(norm1 + norm2)/2.0 * appctx->dt_pre;
    PetscPrintf(PETSC_COMM_SELF, "\tIntegration norm is %g...\n", appctx->integrated_error_norm);
		VecCopy(lam[0],appctx->adj_pre);
    VecCopy(r,appctx->error_pre);
		appctx->dt_pre = dt;
	}
  ierr = VecDestroy(&r);CHKERRQ(ierr);
	return 0;
}

/* ------------------------------------------------------------------- */
/*
   RHSFunction - Evaluates nonlinear function, F(u).

   Input Parameters:
.  ts - the TS context
.  U - input vector
.  ptr - optional user-defined context, as set by TSSetFunction()

   Output Parameter:
.  F - function vector
 */
PetscErrorCode RHSFunction(TS ts,PetscReal ftime,Vec U,Vec F,void *ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  DM             da;
  PetscErrorCode ierr;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      two = 2.0,hx,hy,sx,sy;
  PetscScalar    u,uxx,uyy,**uarray,**f;
  Vec            localU;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);
  c = user->c;
  hx = 1.0/(PetscReal)Mx; sx = 1.0/(hx*hx);
  hy = 1.0/(PetscReal)My; sy = 1.0/(hy*hy);

  /*
     Scatter ghost points to local vector,using the 2-step process
        DMGlobalToLocalBegin(),DMGlobalToLocalEnd().
     By placing code between these two statements, computations can be
     done while messages are in transition.
  */
  ierr = DMGlobalToLocalBegin(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,U,INSERT_VALUES,localU);CHKERRQ(ierr);

  /* Get pointers to vector data */
  ierr = DMDAVecGetArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,F,&f);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      u       = uarray[j][i];
      uxx     = (-two*u + uarray[j][i-1] + uarray[j][i+1])*sx;
      uyy     = (-two*u + uarray[j-1][i] + uarray[j+1][i])*sy;
      f[j][i] = c*(uxx + uyy);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArrayRead(da,localU,&uarray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,F,&f);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&localU);CHKERRQ(ierr);
  ierr = PetscLogFlops(11.0*ym*xm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*
   RHSJacobian - User-provided routine to compute the Jacobian of
   the nonlinear right-hand-side function of the ODE.

   Input Parameters:
   ts - the TS context
   t - current time
   U - global input vector
   dummy - optional user-defined context, as set by TSetRHSJacobian()

   Output Parameters:
   J - Jacobian matrix
   Jpre - optionally different preconditioning matrix
   str - flag indicating matrix structure
*/
PetscErrorCode RHSJacobian(TS ts,PetscReal t,Vec U,Mat J,Mat Jpre,void *ctx)
{
  AppCtx         *user=(AppCtx*)ctx;
  PetscErrorCode ierr;
  DM             da;
  DMDALocalInfo  info;
  PetscInt       i,j;
  PetscReal      hx,hy,sx,sy;
  PetscReal      c = user->c;

  PetscFunctionBeginUser;
  ierr = TSGetDM(ts,&da);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(da,&info);CHKERRQ(ierr);
  hx   = 1.0/(PetscReal)info.mx; sx = 1.0/(hx*hx);
  hy   = 1.0/(PetscReal)info.my; sy = 1.0/(hy*hy);
  for (j=info.ys; j<info.ys+info.ym; j++) {
    for (i=info.xs; i<info.xs+info.xm; i++) {
      PetscInt    nc = 0;
      MatStencil  row,col[5];
      PetscScalar val[5];
      row.i = i; row.j = j;
      col[nc].i = i-1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i+1; col[nc].j = j;   val[nc++] = c*sx;
      col[nc].i = i;   col[nc].j = j-1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j+1; val[nc++] = c*sy;
      col[nc].i = i;   col[nc].j = j;   val[nc++] = c*(-2*sx - 2*sy);
      ierr = MatSetValuesStencil(Jpre,1,&row,nc,col,val,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jpre,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (J != Jpre) {
    ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
PetscErrorCode FormInitialSolution(DM da,Vec U,void* ptr)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscReal      c=user->c;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;
	c  += c;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
PetscErrorCode ExactSolution(DM da, Vec U, void* ptr, PetscReal ftime)
{
  AppCtx         *user=(AppCtx*)ptr;
  PetscErrorCode ierr;
  PetscInt       i,j,xs,ys,xm,ym,Mx,My;
  PetscScalar    **u;
  PetscReal      hx,hy,x,y, c;
  c = user->c;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);CHKERRQ(ierr);

  hx = 1.0/(PetscReal)Mx;
  hy = 1.0/(PetscReal)My;

  /* Get pointers to vector data */
  ierr = DMDAVecGetArray(da,U,&u);CHKERRQ(ierr);

  /* Get local grid boundaries */
  ierr = DMDAGetCorners(da,&xs,&ys,NULL,&xm,&ym,NULL);CHKERRQ(ierr);

  /* Compute function over the locally owned part of the grid */
  for (j=ys; j<ys+ym; j++) {
    y = j*hy;
    for (i=xs; i<xs+xm; i++) {
      x = i*hx;
      u[j][i] = PetscExpReal(-8.0 * PETSC_PI * PETSC_PI *ftime *c) * PetscCosReal(2.0 * PETSC_PI * x) * PetscCosReal(2.0 * PETSC_PI * y);
    }
  }

  /* Restore vectors */
  ierr = DMDAVecRestoreArray(da,U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ComputeEn(PetscInt m, PetscReal *en)
{
	if (m % 2 == 0)
	{
		*en *= 2.0 / PETSC_PI;
		for (int i = 1; i <= m/2 - 1; i++)
		{
			*en *= ((2. * i) / (2. * i - 1.));
		}
	}
	else
	{
		for (int i = 1; i <= (m-1)/2; i++)
		{
			*en *= ((2. * i - 1.) / (2. * i));
		}
	}
	return 0;
}
/*TEST

    test:
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 2
      args: -ts_max_steps 5 -ts_monitor

    test:
      suffix: 3
      args: -ts_max_steps 5 -snes_fd_color -ts_monitor

TEST*/
